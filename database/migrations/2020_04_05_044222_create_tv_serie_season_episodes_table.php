<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTvSerieSeasonEpisodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tv_serie_season_episodes', function (Blueprint $table) {
            $table->id();
            $table->integer('seasons_id')->unsigned();
            $table->string('episode_link')->nullable();
            $table->integer('episode_no')->nullable();
            $table->string('thumbnail')->nullable();
            $table->string('title')->nullable();
            $table->text('detail')->nullable();
            $table->integer('category_id')->nullable();
            $table->char('type')->default('E');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tv_serie_season_episodes');
    }
}
