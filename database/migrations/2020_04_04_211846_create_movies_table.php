<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMoviesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('movies', function (Blueprint $table) {
            $table->id();
            $table->integer('tmdb_id')->nullable();
            $table->string('title')->nullable();
            $table->integer('category_id')->nullable();
            $table->string('movie_link');
            $table->boolean('adult')->nullable();
            $table->string('duration')->nullable();
            $table->string('thumbnail')->nullable();
            $table->string('poster')->nullable();
            $table->integer('collection_id')->nullable();
            $table->string('trailer_url')->nullable();
            $table->string('home_screen')->nullable();
            $table->boolean('is_home_screen')->nullable()->default(false);
            $table->text('detail')->nullable();
            $table->string('status')->nullable()->default('pending');
            $table->integer('rating')->nullable();
            $table->string('released')->nullable();
            $table->char('type')->default('M');
            $table->integer('deleted_by')->nullable();
            $table->integer('created_by')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('movies');
    }
}
