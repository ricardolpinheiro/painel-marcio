<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRevendasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('revendas', function (Blueprint $table) {
            $table->id();
            $table->string('uuid')->nullable()->default('');
            $table->string('name');
            $table->string('username');
            $table->string('obs')->nullable();
            $table->string('password');
            $table->integer('credits')->default(10)->nullable();
            $table->boolean('has_painel')->nullable()->default(0);
            $table->integer('limit_users')->nullable()->default(99999);
            $table->string('path_logo')->nullable();
            $table->string('path_background')->nullable();
            $table->string('url_painel')->nullable();
            $table->string('app_name')->nullable();
            $table->string('app_url')->nullable();
            $table->string('primary_color')->nullable();
            $table->string('secondary_color')->nullable();
            $table->integer('screen_count')->nullable()->default(1);
            $table->boolean('status')->nullable()->default(1);
            $table->integer('own_id')->nullable();
            $table->integer('created_by')->nullable();
            $table->dateTime('expired_at')->nullable();
            $table->boolean('is_trial')->nullable()->default(0);
            $table->integer('deleted_by')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('revendas');
    }
}
