<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEpgChannelItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('epg_channel_items', function (Blueprint $table) {
            $table->id();
            $table->integer("id_canal");
            $table->string("st_titulo");
            $table->timestamp("dh_inicio")->nullable()->default(now());
            $table->timestamp("dh_fim")->nullable()->default(now());
            $table->integer("id_programa");
            $table->string("titulo");
            $table->integer('deleted_by')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('epg_channel_items');
    }
}
