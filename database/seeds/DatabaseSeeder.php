<?php

use App\Category;
use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);

        $user = User::where('username', '=', 'ricardo')->get();

        if(!$user->count()) {
            User::create([
                'name' => "Ricardo",
                'username' => "ricardo",
                'password' => Hash::make("ricardo"),
            ]);
        }

        $user = User::where('username', '=', 'mauricio')->get();
        if(!$user->count()) {
            User::create([
                'name' => "mauricio",
                'username' => "mauricio",
                'password' => Hash::make("mauricio"),
            ]);
        }

        $user = User::where('username', '=', 'marcio')->get();
        if(!$user->count()) {
            User::create([
                'name' => "marcio",
                'username' => "marcio",
                'password' => Hash::make("marcio"),
            ]);
        }

        $category = Category::where('name', '=', 'IFrame URL')->get();
        if(!$category->count()) {
            Category::create([
                'name' => "IFrame URL",
                'status' => 1,
            ]);
        }

        $category = Category::where('name', '=', 'Youtube Api')->get();
        if(!$category->count()) {
            Category::create([
                'name' => "Youtube Api",
                'status' => 1,
            ]);
        }

        $category = Category::where('name', '=', 'Vimeo Api')->get();
        if(!$category->count()) {
            Category::create([
                'name' => "Vimeo Api",
                'status' => 1,
            ]);
        }

        $category = Category::where('name', '=', 'Twitch Api')->get();
        if(!$category->count()) {
            Category::create([
                'name' => "Twitch Api",
                'status' => 1,
            ]);
        }

    }
}
