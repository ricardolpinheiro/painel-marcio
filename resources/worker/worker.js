const { exec } = require("child_process");
var schedule = require('node-schedule');



var j = schedule.scheduleJob('* * * * *', async function(){
    await exec("ffprobe -v quiet -print_format json -show_streams -show_streams http://api.new.livestream.com/accounts/15107153/events/4338771/live.m3u8", (error, stdout, stderr) => {
        if (error) {
            console.log(`error: ${error.message}`);
            return;
        }
        if (stderr) {
            console.log(`stderr: ${stderr}`);
            return;
        }
        console.log(`stdout: ${stdout}`);
    });
});
