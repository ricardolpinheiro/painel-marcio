@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-lg-6">
            <div class="element-wrapper">
                <div class="element-actions">
                    <a href="{{ route('users.index') }}" type="button" class="btn btn-outline-secondary min-width-125 pull-right">Voltar</a>
                </div>
                <h6 class="element-header">
                    Cadastro de Usuários
                </h6>
                <div class="element-content">
                    <div class="element-box">
                        {!!  Form::open(['route' => ['users.store']]) !!}
                            @csrf

                            <x-input name="name" text="Nome" placeholder="Digite o Nome" :error="$errors"/>

                            <x-input name="username" text="Username" placeholder="Digite o Username" :error="$errors"/>

                            <x-input name="password" text="Senha" placeholder="Digite a Senha" :error="$errors" />

                            <button type="submit" class="btn btn-primary min-width-125">Cadastrar</button>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
