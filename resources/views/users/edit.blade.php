@extends('layouts.app')

@section('content')
    <x-message :message="Session::has('success') ? Session::get('success') : ''"/>

    <div class="row">
        <div class="col-lg-6">
            <div class="element-wrapper">
                <div class="element-actions">
                    <a href="{{ route('users.index') }}" type="button" class="btn btn-outline-secondary min-width-125 pull-right">Voltar</a>
                </div>
                <h6 class="element-header">
                    Edição de Usuários
                </h6>

                <div class="element-content">
                    <div class="element-box">
                        {!!  Form::model($user, ['method' => 'PUT','route' => ['users.update', $user->id]]) !!}
                            @csrf

                            <x-input name="name" text="Nome" placeholder="Digite o Nome" :value="$user->name" :error="$errors"/>

                            <x-input name="username" text="Username" placeholder="Digite o Username" :value="$user->username" :error="$errors"/>

                            <x-select2 name="status" text="Status" :value="$user->status" placeholder="Selecione o Status" :error="$errors"
                                   :options="$status"/>

                            <button type="submit" class="btn btn-primary min-width-125">Salvar</button>

                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
