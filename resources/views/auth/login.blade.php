@extends('layouts.auth')

@section('content')
<div class="auth-box-w">
	<div class="logo-w">
	  <a href="index.html"></a>
	</div>
	<h4 class="auth-header">
	  {{ __('Login') }}
	</h4>
	<form method="POST" action="{{ route('login') }}">
		@csrf

	  <div class="form-group @error('username') has-error has-danger @enderror">

		<label for="">{{ __('Username') }}</label>
		<input name="username" class="form-control" placeholder="Digite seu username.." type="text" value="{{ old('username') }}" required autocomplete="username" autofocus>
		<div class="pre-icon os-icon os-icon-user-male-circle"></div>
		@error('username')
			<div class="help-block form-text text-muted form-control-feedback">{{ $message }}</div>
		@enderror
	  </div>
	  <div class="form-group @error('password') has-error has-danger @enderror">
		<label for="">{{ __('Password') }}</label>
		<input id="password" type="password" placeholder="Digite sua senha.." class="form-control" name="password" required autocomplete="current-password">
		<div class="pre-icon os-icon os-icon-fingerprint"></div>
		@error('password')
			<div class="help-block form-text text-muted form-control-feedback">{{ $message }}</div>
		@enderror
	  </div>
	  <div class="buttons-w">
		<button type="submit" class="btn btn-primary">Log in</button>
		<div class="form-check-inline">
		<label class="form-check-label">
			<input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
			{{ __('Remember Me') }}
		 </label>
		</div>
	  </div>
	</form>
</div>
@endsection
