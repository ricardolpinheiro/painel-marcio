@extends('layouts.auth')

@section('content')
<div class="auth-box-w">
	<form method="POST" action="{{ route('register') }}">
		@csrf
        <x-input name="name" text="Nome" placeholder="Digite o Nome" icon="os-icon-user-male-circle" :error="$errors"/>

        <x-input name="username" text="Username" placeholder="Digite o Username" icon="os-icon-email-2-at2" :error="$errors"/>

        <x-input name="password" text="Senha" placeholder="Digite sua Senha" icon="os-icon-fingerprint" :error="$errors"/>

        <x-input name="password_confirmation" text="Confirmação de Senha" icon="os-icon-fingerprint" placeholder="Confirme sua Senha" :error="$errors"/>

		<div class="form-group row mb-0">
			<div class="col-md-6 offset-md-4">
				<button type="submit" class="btn btn-primary">
					{{ __('Register') }}
				</button>
			</div>
		</div>
    </form>
</div>

@endsection
