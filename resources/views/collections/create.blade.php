@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-lg-6">
            <div class="element-wrapper">
                <div class="element-actions">
                    <a href="{{ route('collections.index') }}" type="button" class="btn btn-outline-secondary min-width-125 pull-right">Voltar</a>
                </div>
                <h6 class="element-header">
                    Cadastro de Collection
                </h6>
                <div class="element-content">
                    <div class="element-box">
                        {!!  Form::open(['route' => ['collections.store'], 'enctype' => 'multipart/form-data']) !!}
                            @csrf

                            <x-input name="tmdb_id" text="TMDB ID" placeholder="TMDB ID" :error="$errors"/>

                            <x-input name="name" text="Nome" placeholder="Digite o Nome" :error="$errors"/>

                            <x-text-area name="overview" text="Overview" placeholder="Digite o texto" :error="$errors"/>

                            <x-input-file name="poster_path" text="Poster" placeholder="" :error="$errors"/>

                            <x-input-file name="backdrop_path" text="Backdrop Path" placeholder="" :error="$errors"/>

                        <button type="submit" class="btn btn-primary min-width-125">Cadastrar</button>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
