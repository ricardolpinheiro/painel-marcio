@extends('layouts.app')

@section('content')
    <div class="element-wrapper">
        <div class="element-actions">
            <a href="{{ route('collections.create') }}" type="button" class="btn btn-primary min-width-125 pull-right">Cadastrar Coleção</a>
        </div>
        <h6 class="element-header">
            Gerenciar Coleção
        </h6>

        <div class="element-content">
            <table id="dataTable1" class="table table-bordered table-striped table-vcenter js-dataTable-full">
                <thead>
                <tr>
                    <th class="text-center">ID</th>
                    <th class="text-center">Tmdb ID</th>
                    <th class="text-center">Thumb</th>
                    <th class="text-center">Poster</th>
                    <th class="text-center">Nome</th>
                    <th class="text-center">Filmes</th>
                    <th class="text-center"></th>
                </tr>
                </thead>
                <tbody>
                    @foreach($collections as $collection)
                        <tr>
                            <td class="text-center">{{ $collection->id }}</td>
                            <td class="text-center">{{ $collection->tmdb_id }}</td>
                            <td class="text-center">
                                <x-image-cell :url="$collection->backdrop_path"/>
                            </td>
                            <td class="text-center">
                                <x-image-cell :url="$collection->poster_path"/>
                            </td>
                            <td class="text-center">{{ $collection->name }}</td>
                            <td class="text-center">{{ $collection->movie->count() }}</td>
                            <td>
                                <x-button :route="route('collections.edit', $collection->id)" type="info" icon="os-icon-edit" tooltip="Editar Collection" />
                                <button type="button" class="btn btn-danger" data-toggle="tooltip"  title="Deletar" onclick="deletResource({{$collection->id}})">
                                    <i class="os-icon os-icon-x"></i>
                                </button>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>


@endsection

@section('after-script')

    <form action="" id="id-form-delete" method="POST" style="display: none">
        @method('DELETE')
        @csrf
    </form>

    <script>
        @if(Session::has('success'))
        Swal.fire(
            'Suecesso!',
            '{{Session::get('success')}}',
            'success'
        );
        @endif
        function deletResource(id) {
            Swal.fire({
                title: 'Tem certeza?',
                text: "O Recurso será deletado da base",
                icon: 'warning',
                showCancelButton: true,
                cancelButtonText: "Cancelar",
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Sim Deletar'
            }).then((result) => {
                if (result.value) {
                    $("#id-form-delete").attr('action', '/collections/' + id);
                    $("#form-status").val(status);
                    $("#id-form-delete").submit();
                }
            })
        }
    </script>
@endsection
