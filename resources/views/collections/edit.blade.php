@extends('layouts.app')

@section('content')
    <x-message :message="Session::has('success') ? Session::get('success') : ''"/>

    <div class="row">
        <div class="col-lg-6">
            <div class="element-wrapper">
                <div class="element-actions">
                    <a href="{{ route('collections.index') }}" type="button" class="btn btn-outline-secondary min-width-125 pull-right">Voltar</a>
                </div>
                <h6 class="element-header">
                    Edição de Bouquets
                </h6>

                <div class="element-content">
                    <div class="element-box">
                        {!!  Form::model($collection, ['method' => 'PUT','route' => ['collections.update', $collection->id]]) !!}
                        @csrf

                        <x-input name="tmdb_id" text="TMDB ID" :value="$collection->tmdb_id" placeholder="TMDB ID" :error="$errors"/>

                        <x-input name="name" text="Nome" :value="$collection->name" placeholder="Digite o Nome" :error="$errors"/>

                        <x-text-area name="overview" text="Overview" :value="$collection->overview" placeholder="Digite o texto" :error="$errors"/>

                        <h6><b>Thumbnail</b></h6>
                        <div class="profile-tile">
                            <a class="profile-tile-box" href="{{ $collection->backdrop_path }}" target="_blank">
                                <div class="pt-avatar-w">
                                    <img alt="" src="{{ $collection->backdrop_path }}">
                                </div>
                            </a>
                            <div class="pt-btn">
                                <button type="button" class="btn btn-secondary btn-sm" onclick="$('#backdrop_path').trigger('click');">Alterar</button>
                            </div>
                        </div>
                        <x-input-file display="none" name="backdrop_path" text="" placeholder="" :error="$errors"/>

                        <h6><b>Poster</b></h6>
                        <div class="profile-tile">
                            <a class="profile-tile-box" href="{{ $collection->poster_path }}" target="_blank">
                                <div class="pt-avatar-w">
                                    <img alt="" src="{{ $collection->poster_path }}">
                                </div>
                            </a>
                            <div class="pt-btn">
                                <button type="button" class="btn btn-secondary btn-sm" onclick="$('#poster_path').trigger('click');">Alterar</button>
                            </div>
                        </div>

                        <x-input-file display="none" name="poster_path" text="" placeholder="" :error="$errors"/>


                        <button type="submit" class="btn btn-primary min-width-125">Salvar</button>

                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
