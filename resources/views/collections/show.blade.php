@extends('layouts.app')

@section('content')
    <div class="element-wrapper">
        <div class="element-actions">
            <a href="{{ route('collections.index') }}" type="button" class="btn btn-outline-secondary min-width-125 pull-right">Voltar</a>
        </div>
        <h6 class="element-header">
            Gerenciar Coleção
        </h6>

        <div class="element-content">
            <table id="dataTable1" class="table table-bordered table-striped table-vcenter js-dataTable-full">
                <thead>
                <tr>
                    <th class="text-center">ID</th>
                    <th class="text-center">Name</th>
                    <th class="text-center">Thumb</th>
                </tr>
                </thead>
                <tbody>
                    @foreach($collection->movie as $c)
                        <tr>
                            <td class="text-center">{{ $c->id }}</td>
                            <td class="text-center">{{ $c->title }}</td>
                            <td class="text-center">
                                <x-image-cell :url="$c->poster"/>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>


@endsection
