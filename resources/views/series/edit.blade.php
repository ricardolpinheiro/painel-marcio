@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-lg-6">
            <div class="element-wrapper">
                <div class="element-actions">
                    <a href="{{ route('series.index') }}" type="button" class="btn btn-outline-secondary min-width-125 pull-right">Voltar</a>
                </div>
                <h6 class="element-header">
                    Edição de Séries de TV
                </h6>

                <div class="element-content">
                    <div class="element-box">
                        {!!  Form::model($serie, ['method' => 'PUT','route' => ['series.update', $serie->id], 'enctype' => 'multipart/form-data']) !!}
                        @csrf

                        <x-input name="title" text="Título" :value="$serie->title" placeholder="Digite o Título" :error="$errors"/>
                        <x-input name="tmdb_id" text="TMDB ID" :value="$serie->tmdb_id" placeholder="Digite o TMDB ID" :error="$errors"/>

                        <x-multiselect name="bouquet_id[]" text="Bouquets" :value="$serie->available_bouquets" placeholder="Selecione o(s) Bouquet(s)" :error="$errors"
                                       :options="$bouquets"/>

                        <x-text-area name="detail" text="Overview" :value="$serie->detail" placeholder="Digite o texto" :error="$errors"/>


                        <x-input type="number" name="rating" text="Rating" :value="$serie->rating" placeholder="Digite o Título" :error="$errors"/>

                        <h6><b>Poster</b></h6>
                        <div class="profile-tile">
                            <a class="profile-tile-box" href="{{ $serie->poster_path }}" target="_blank">
                                <div class="pt-avatar-w">
                                    <img alt="" src="{{ $serie->poster_path }}">
                                </div>
                            </a>
                            <div class="pt-btn">
                                <button type="button" class="btn btn-secondary btn-sm" onclick="$('#poster_path').trigger('click');">Alterar</button>
                            </div>
                        </div>
                        <x-input-file display="none" name="poster_path" text="" placeholder="" :error="$errors"/>

                        <button type="submit" class="btn btn-primary min-width-125">Salvar</button>

                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection
