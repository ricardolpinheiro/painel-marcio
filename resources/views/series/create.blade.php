@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-lg-6">
            <div class="element-wrapper">
                <div class="element-actions">
                    <a href="{{ route('series.index') }}" type="button" class="btn btn-outline-secondary min-width-125 pull-right">Voltar</a>
                </div>
                <h6 class="element-header">
                    Cadastro de Séries de TV
                </h6>

                <div class="element-content">
                    <div class="element-box">
                        {!!  Form::open(['route' => ['series.store'], 'enctype' => 'multipart/form-data']) !!}
                            @csrf

                            <x-input name="title" text="Título" placeholder="Digite o Título" :error="$errors"/>
                        
                            <x-input name="tmdb_id" text="TMDB ID" placeholder="Digite o TMDB ID" :error="$errors"/>

                            <x-multiselect name="bouquet_id[]" text="Bouquets" placeholder="Selecione o(s) Bouquet(s)" :error="$errors"
                                           :options="$bouquets"/>

                            <x-text-area name="detail" text="Overview" placeholder="Digite o texto" :error="$errors"/>

                            <x-input type="number" name="rating" text="Rating" placeholder="Digite o Título" :error="$errors"/>

                            <x-input-file name="poster_path" text="Poster" placeholder="" :error="$errors"/>

                        <button type="submit" class="btn btn-primary min-width-125">Cadastrar</button>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
