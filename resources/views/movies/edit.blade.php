@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-lg-6">
            <div class="element-wrapper">
                <div class="element-actions">
                    <a href="{{ route('movies.index') }}" type="button" class="btn btn-outline-secondary min-width-125 pull-right">Voltar</a>
                </div>
                <h6 class="element-header">
                    Edição de Filmes
                </h6>
                <div class="element-content">
                    <div class="element-box">
                        {!!  Form::model($movie, ['method' => 'PUT','route' => ['movies.update', $movie->id], 'enctype' => 'multipart/form-data']) !!}
                        @csrf

                        <x-input name="tmdb_id" text="TMDB ID" :value="$movie->tmdb_id" placeholder="TMDB ID" :error="$errors"/>

                        <x-input name="title" text="Título" :value="$movie->title" placeholder="Digite o Titulo" :error="$errors"/>

                        <x-select2 name="category_id" text="Categoria" :value="$movie->category_id" placeholder="Selecione a Categoria" :error="$errors"
                                   :options="$categories"/>


                        <x-multiselect name="bouquet_id[]" text="Bouquets" :value="$movie->available_bouquets" placeholder="Selecione o(s) Bouquet(s)" :error="$errors"
                                       :options="$bouquets"/>

                        <x-select2 name="collection_id" text="Collection" :value="$movie->collection_id" placeholder="Selecione a Collection" :error="$errors"
                                   :options="$collections"/>

                        <x-input name="movie_link" text="Link do Filme" :value="$movie->movie_link" placeholder="Digite Link do Filme" :error="$errors"/>

                        <x-input name="trailer_url" text="Trailer do Filme" :value="$movie->trailer_url" placeholder="Digite Trailer do Filme" :error="$errors"/>

                        <x-input type="number" name="rating" text="Avaliação" :value="$movie->rating" placeholder="Digite a Avaliação do Filme" :error="$errors"/>

                        <x-input name="released" text="Lançamento" :value="$movie->released" placeholder="Data de Lançamento" :error="$errors"/>

                        <x-text-area name="detail" text="Overview" :value="$movie->detail" placeholder="Digite o texto" :error="$errors"/>

                        <h6><b>Thumbnail</b></h6>
                        <div class="profile-tile">
                            <a class="profile-tile-box" href="{{ $movie->thumbnail }}" target="_blank">
                                <div class="pt-avatar-w">
                                    <img alt="" src="{{ $movie->thumbnail }}">
                                </div>
                            </a>
                            <div class="pt-btn">
                                <button type="button" class="btn btn-secondary btn-sm" onclick="$('#thumbnail').trigger('click');">Alterar</button>
                            </div>
                        </div>
                        <x-input-file display="none" name="thumbnail" text="" placeholder="" :error="$errors"/>

                        <h6><b>Poster</b></h6>
                        <div class="profile-tile">
                            <a class="profile-tile-box" href="{{ $movie->poster }}" target="_blank">
                                <div class="pt-avatar-w">
                                    <img alt="" src="{{ $movie->poster }}">
                                </div>
                            </a>
                            <div class="pt-btn">
                                <button type="button" class="btn btn-secondary btn-sm" onclick="$('#poster').trigger('click');">Alterar</button>
                            </div>
                        </div>

                        <x-input-file display="none" name="poster" text="" placeholder="" :error="$errors"/>



                        <button type="submit" class="btn btn-primary min-width-125">Salvar</button>

                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
