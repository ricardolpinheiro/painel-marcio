@extends('layouts.app')


@section('content')

    <div class="element-wrapper">
        <div class="element-actions">
            <a href="{{ route('movies.create') }}" type="button" class="btn btn-primary min-width-125 pull-right">Cadastrar Filmes</a>
        </div>
        <h6 class="element-header">
            Gerenciar Filmes
        </h6>
        <div class="element-content">
            <table id="dataTable1" class="table table-bordered table-striped table-vcenter js-dataTable-full">
                <thead>
                <tr>
                    <th class="text-center">ID</th>
                    <th class="text-center">TMDB</th>
                    <th class="text-center">Thumbail</th>
                    <th class="text-center">Poster</th>
                    <th class="text-center">Nome</th>
                    <th class="text-center">Status</th>
                    <th class="text-center">Coleção</th>
                    <th class="text-center"></th>
                </tr>
                </thead>
                <tbody>
                    @foreach($movies as $movie)
                        <tr>

                            <td class="text-center">{{ $movie->id }}</td>
                            <td class="text-center">{{ $movie->tmdb_id }}</td>
                            <td class="text-center">
                                <x-image-cell :url="$movie->thumbnail"/>
                            </td>
                            <td class="text-center">
                                <x-image-cell :url="$movie->poster"/>
                            </td>
                            <td class="text-center">{{ $movie->title }}</td>
                            <td class="text-center">
                                @if($movie->status === 'online')
                                    <x-bedge type="success" text="Online" />
                                @elseif($movie->status === 'offline')
                                    <x-bedge type="warning" text="Offline" />
                                @else
                                    <x-bedge type="danger" :text="$movie->status" />
                                @endif
                            </td>
                            <td class="text-center">
                                @if(!empty($movie->collection_id))
                                <a href="{{ route('collections.show', $movie->collection_id) }}">
                                    {{ $movie->collection->name ?? '' }}
                                </a>
                                @else
                                    n/a
                                @endif
                            </td>
                            <td>
                                <x-button :route="route('movies.edit', $movie->id)" type="info" icon="os-icon-film" tooltip="Editar Filme" />
                                @if($movie->status == 'pending-aprove' || $movie->status == 'offline')
                                    <button type="button" class="btn btn-success" data-toggle="tooltip"  title="Alterar par Online" onclick="setStatusMovie('online', {{$movie->id}})">
                                        <i class="os-icon os-icon-video"></i>
                                    </button>
                                @else
                                    <button type="button" class="btn btn-warning" data-toggle="tooltip"  title="Alterar par Offline" onclick="setStatusMovie('offline', {{$movie->id}})">
                                        <i class="os-icon os-icon-video-off"></i>
                                    </button>
                                @endif
                                <button type="button" class="btn btn-danger" data-toggle="tooltip"  title="Deletar" onclick="deleteMovie({{$movie->id}})">
                                    <i class="os-icon os-icon-x"></i>
                                </button>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <form action="" id="id-form" method="POST" style="display: none">
        @method('PUT')
        @csrf
        <input type="text" value="online" name="status" id="form-status">
    </form>
    <form action="" id="id-form-delete" method="POST" style="display: none">
        @method('DELETE')
        @csrf
    </form>
@endsection


@section('after-script')
    <script>
        @if(Session::has('success'))
            Swal.fire(
                'Suecesso!',
                '{{Session::get('success')}}',
                'success'
            );
        @endif
        function setStatusMovie(status, id) {
            Swal.fire({
                title: 'Tem certeza?',
                text: "Você esta alterando este filme para " + status,
                icon: 'warning',
                showCancelButton: true,
                cancelButtonText: "Cancelar",
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Sim Alterar'
            }).then((result) => {
                if (result.value) {
                    $("#id-form").attr('action', '/movies/' + id);
                    $("#form-status").val(status);
                    $("#id-form").submit();
                }
            })
        }
        function deleteMovie(id) {
            Swal.fire({
                title: 'Tem certeza?',
                text: "O Filme será deletado da base",
                icon: 'warning',
                showCancelButton: true,
                cancelButtonText: "Cancelar",
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Sim Deletar'
            }).then((result) => {
                if (result.value) {
                    $("#id-form-delete").attr('action', '/movies/' + id);
                    $("#form-status").val(status);
                    $("#id-form-delete").submit();
                }
            })
        }
    </script>
@endsection
