@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-lg-6">
            <div class="element-wrapper">
                <div class="element-actions">
                    <a href="{{ route('movies.index') }}" type="button" class="btn btn-outline-secondary min-width-125 pull-right">Voltar</a>
                </div>
                <h6 class="element-header">
                    Cadastro de Filmes
                </h6>
                <div class="element-content">
                    <div class="element-box">
                        {!!  Form::open(['route' => ['movies.store']]) !!}
                            @csrf

                            <x-input name="title" text="Título" placeholder="Digite o Titulo do Filme" :error="$errors"/>

                            <x-input name="tmdb_id" text="TMDB ID" placeholder="ID do Filme no TMDB" :error="$errors"/>

                            <x-multiselect name="bouquet_id[]" text="Bouquets" placeholder="Selecione o(s) Bouquet(s)" :error="$errors"
                                           :options="$bouquets"/>

                            <x-select2 name="category_id" text="Categoria" placeholder="Selecione a Categoria" :error="$errors"
                                       :options="$categories"/>

                            <x-input-prepend prepend="http://" name="movie_link" text="Filme URL" placeholder="Insira a URL do Filme" :error="$errors"/>

                            <button type="submit" class="btn btn-primary min-width-125">Cadastrar</button>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
