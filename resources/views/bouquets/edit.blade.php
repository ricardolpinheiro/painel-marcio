@extends('layouts.app')

@section('content')
    <x-message :message="Session::has('success') ? Session::get('success') : ''"/>

    <div class="row">
        <div class="col-lg-6">
            <div class="element-wrapper">
                <div class="element-actions">
                    <a href="{{ route('bouquets.index') }}" type="button" class="btn btn-outline-secondary min-width-125 pull-right">Voltar</a>
                </div>
                <h6 class="element-header">
                    Edição de Bouquets
                </h6>

                <div class="element-content">
                    <div class="element-box">
                        {!!  Form::model($bouquet, ['method' => 'PUT','route' => ['bouquets.update', $bouquet->id]]) !!}
                        @csrf

                        <x-input name="name" text="Nome" :value="$bouquet->name" placeholder="Digite o Nome" :error="$errors"/>

                        <x-select2 name="status" text="Status" placeholder="Selecione o Status" :error="$errors"
                                   :options="array('1' => 'Ativo', '0' =>'Inativo')"/>

                        <button type="submit" class="btn btn-primary min-width-125">Salvar</button>

                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
