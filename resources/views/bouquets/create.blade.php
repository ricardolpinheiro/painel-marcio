@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-lg-6">
            <div class="element-wrapper">
                <div class="element-actions">
                    <a href="{{ route('bouquets.index') }}" type="button" class="btn btn-outline-secondary min-width-125 pull-right">Voltar</a>
                </div>
                <h6 class="element-header">
                    Cadastro de Bouquets
                </h6>
                <div class="element-content">
                    <div class="element-box">
                        {!!  Form::open(['route' => ['bouquets.store']]) !!}
                            @csrf

                            <x-input name="name" text="Nome" placeholder="Digite o Nome" :error="$errors"/>

                            <x-select2 name="status" text="Status" placeholder="Selecione o Status" :error="$errors"
                                       :options="array('1' => 'Ativo', '0' =>'Inativo')"/>

                            <button type="submit" class="btn btn-primary min-width-125">Cadastrar</button>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
