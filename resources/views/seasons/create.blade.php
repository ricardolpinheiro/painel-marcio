@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-lg-6">
            <div class="element-wrapper">
                <div class="element-actions">
                    <a href="{{ route('seasons.index') }}" type="button" class="btn btn-outline-secondary min-width-125 pull-right">Voltar</a>
                </div>
                <h6 class="element-header">
                    Cadastro de Temporada de TV
                </h6>

                <div class="element-content">
                    <div class="element-box">
                        {!!  Form::open(['route' => ['seasons.store'], 'enctype' => 'multipart/form-data']) !!}
                            @csrf

                            <x-input type="number" name="season_no" text="Season" placeholder="Digite a Season" :error="$errors"/>

                            <x-select2 name="tv_series_id" text="Série" placeholder="Selecione a Série de TV" :error="$errors"
                                   :options="$series"/>

                            <x-text-area name="detail" text="Overview" placeholder="Digite o texto" :error="$errors"/>

                            <x-input-file name="poster_path" text="Poster" placeholder="" :error="$errors"/>

                             <button type="submit" class="btn btn-primary min-width-125">Cadastrar</button>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
