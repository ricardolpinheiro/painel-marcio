@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-lg-6">
            <div class="element-wrapper">
                <div class="element-actions">
                    <a href="{{ route('seasons.index') }}" type="button" class="btn btn-outline-secondary min-width-125 pull-right">Voltar</a>
                </div>
                <h6 class="element-header">
                    Edição de Season
                </h6>

                <div class="element-content">
                    <div class="element-box">
                        {!!  Form::model($season, ['method' => 'PUT','route' => ['seasons.update', $season->id], 'enctype' => 'multipart/form-data']) !!}
                        @csrf

                        <x-input type="number" name="season_no" text="Season" :value="$season->season_no" placeholder="Digite a Season" :error="$errors"/>

                        <x-text-area name="detail" text="Overview" :value="$season->detail" placeholder="Digite o texto" :error="$errors"/>

                        <x-select2 name="tv_series_id" text="Série" placeholder="Selecione a Série de TV" :value="$season->tv_series_id" :error="$errors"
                                   :options="$series"/>

                        <h6><b>Poster</b></h6>
                        <div class="profile-tile">
                            <a class="profile-tile-box" href="{{ $season->poster_path }}" target="_blank">
                                <div class="pt-avatar-w">
                                    <img alt="" src="{{ $season->poster_path }}">
                                </div>
                            </a>
                            <div class="pt-btn">
                                <button type="button" class="btn btn-secondary btn-sm" onclick="$('#poster_path').trigger('click');">Alterar</button>
                            </div>
                        </div>
                        <x-input-file display="none" name="poster_path" text="" placeholder="" :error="$errors"/>

                        <button type="submit" class="btn btn-primary min-width-125">Salvar</button>

                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
