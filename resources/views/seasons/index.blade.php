@extends('layouts.app')

@section('content')

    <div class="element-wrapper">
        <div class="element-actions">
            <a href="{{ route('seasons.create') }}" type="button" class="btn btn-primary min-width-125 pull-right">Cadastrar Season</a>
        </div>
        <h6 class="element-header">
            Gerenciar Temporadas de Séries de TV
        </h6>

        <div class="element-content">
            <table id="dataTable1" class="table table-bordered table-striped table-vcenter js-dataTable-full">
                <thead>
                <tr>
                    <th class="text-center"></th>
                    <th class="text-center">Titulo</th>
                    <th>Poster</th>
                    <th class="d-none d-sm-table-cell">Temporada</th>
                    <th class="d-none d-sm-table-cell">Episódios</th>
                    <th class="text-center" style="width: 15%;"></th>
                </tr>
                </thead>
                <tbody>
                    @foreach($seasons as $season)
                        <tr>
                            <td class="text-center">{{ $season->id }}</td>
                            <td class="text-center">{{ $season->serie->title ?? '' }}</td>
                            <td class="text-center">
                                <x-image-cell :url="$season->poster_path"/>
                            </td>
                            <td class="text-center">Season {{ $season->season_no }}</td>
                            <td>{{$season->episodes->count()}}</td>
                            <td class="text-center">
                                <x-button :route="route('seasons.edit', $season->id)" type="info" icon="os-icon-edit" tooltip="Editar Series" />
                                <button type="button" class="btn btn-danger" data-toggle="tooltip" title="Deletar" onclick="deletResource({{$season->id}})">
                                    <i class="os-icon os-icon-trash-2"></i>
                                </button>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>


@endsection


@section('after-script')

    <form action="" id="id-form-delete" method="POST" style="display: none">
        @method('DELETE')
        @csrf
    </form>

    <script>
        @if(Session::has('success'))
        Swal.fire(
            'Suecesso!',
            '{{Session::get('success')}}',
            'success'
        );
        @endif
        function deletResource(id) {
            Swal.fire({
                title: 'Tem certeza?',
                text: "O Recurso será deletado da base",
                icon: 'warning',
                showCancelButton: true,
                cancelButtonText: "Cancelar",
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Sim Deletar'
            }).then((result) => {
                if (result.value) {
                    $("#id-form-delete").attr('action', '/seasons/' + id);
                    $("#form-status").val(status);
                    $("#id-form-delete").submit();
                }
            })
        }
    </script>
@endsection
