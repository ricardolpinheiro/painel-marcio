@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-lg-6">
            <div class="element-wrapper">
                <div class="element-actions">
                    <a href="{{ route('episodes.index') }}" type="button" class="btn btn-outline-secondary min-width-125 pull-right">Voltar</a>
                </div>
                <h6 class="element-header">
                    Edição de Episódio de TV
                </h6>
                <div class="element-content">
                    <div class="element-box">
                        {!!  Form::model($episode, ['method' => 'PUT','route' => ['episodes.update', $episode->id]]) !!}
                        @csrf

                        <x-input name="title" text="Titulo" :value="$episode->title" placeholder="Digite o Titulo" :error="$errors"/>

                        <x-select2 name="seasons_id" text="Temporada" :value="$episode->seasons_id" placeholder="Selecione a Temporada" :error="$errors"
                                   :options="$seasons"/>

                        <x-input type="number" name="episode_no" text="Episódio" :value="$episode->episode_no" placeholder="Digite o Episódio" :error="$errors"/>

                        <x-input name="episode_link" text="Link do Episído" :value="$episode->episode_link" placeholder="Digite link do Episódio" :error="$errors"/>

                        <x-select2 name="category_id" text="Categoria" :value="$episode->category_id" placeholder="Selecione a Categoria" :error="$errors"
                                   :options="$categories"/>

                        <x-text-area name="detail" text="Overview" :value="$episode->detail" placeholder="Digite o texto" :error="$errors"/>

                        <button type="submit" class="btn btn-primary min-width-125">Salvar</button>

                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
