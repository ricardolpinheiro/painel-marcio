@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-lg-6">
            <div class="element-wrapper">
                <div class="element-actions">
                    <a href="{{ route('seasons.index') }}" type="button" class="btn btn-outline-secondary min-width-125 pull-right">Voltar</a>
                </div>
                <h6 class="element-header">
                    Cadastro de Episódio de TV
                </h6>
                <div class="element-content">
                    <div class="element-box">
                        {!!  Form::open(['route' => ['episodes.store'], 'enctype' => 'multipart/form-data']) !!}
                            @csrf
                            <x-input name="title" text="Titulo" placeholder="Digite o Titulo" :error="$errors"/>

                            <x-input type="number" name="episode_no" text="Episódio" placeholder="Digite o Episódio" :error="$errors"/>

                            <x-select2 name="seasons_id" text="Temporada" placeholder="Selecione a Temporada" :error="$errors"
                                   :options="$seasons"/>

                            <x-input name="episode_link" text="Link do Episído" placeholder="Digite link do Episódio" :error="$errors"/>

                            <x-select2 name="category_id" text="Categoria" placeholder="Selecione a Categoria" :error="$errors"
                                       :options="$categories"/>

                            <x-text-area name="detail" text="Overview" placeholder="Digite o texto" :error="$errors"/>

                            <button type="submit" class="btn btn-primary min-width-125">Cadastrar</button>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
