@extends('layouts.app')

@section('content')
    <x-message :message="Session::has('success') ? Session::get('success') : ''"/>

    <div class="element-wrapper">
        <div class="element-actions">
            <a href="{{ route('revendas.create') }}" type="button" class="btn btn-primary min-width-125 pull-right">Cadastrar Revenda</a>
        </div>
        <h6 class="element-header">
            Gerenciar Revendas
        </h6>

        <div class="element-content">
            <table id="dataTable1" class="table table-bordered table-striped table-vcenter js-dataTable-full">
                <thead>
                <tr>
                    <th class="text-center"></th>
                    <th class="text-center">UUID</th>
                    <th>Nome</th>
                    <th class="d-none d-sm-table-cell">Username</th>
                    <th class="d-none d-sm-table-cell" style="width: 15%;">Status</th>
                    <th class="d-none d-sm-table-cell" style="width: 15%;">Creditos</th>
                    <th class="text-center" style="width: 25%;"></th>
                </tr>
                </thead>
                <tbody>
                @foreach($revendas as $revenda)
                    <tr>
                        <td class="text-center">{{ $revenda->id }}</td>
                        <td class="text-center">
                            {{ $revenda->uuid }}
                        </td>
                        <td class="text-center">
                            {{ $revenda->name }}
                        </td>
                        <td class="text-center">
                            {{ $revenda->username }}
                        </td>
                        <td class="text-center">
                            <x-status-table :status="$revenda->status"/>

                        </td>
                        <td class="text-center">
                            {{ $revenda->credits }}
                        </td>
                        <td class="text-center">
                            <x-button :route="route('revendas.edit', $revenda->id)" type="info" icon="os-icon-edit-32" tooltip="Editar Revenda" />
                            <button type="button" class="btn btn-warning" data-toggle="tooltip" title="Alterar Senha" onclick="setPassword({{$revenda->id}})">
                                <i class="os-icon os-icon-lock"></i>
                            </button>
                            <button type="button" class="btn btn-danger" data-toggle="tooltip" title="Deletar" onclick="deletResource({{$revenda->id}})">
                                <i class="os-icon os-icon-trash-2"></i>
                            </button>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection


@section('after-script')

    <form action="" id="id-form-delete" method="POST" style="display: none">
        @method('DELETE')
        @csrf
    </form>

    <form action="" id="id-form-update" method="POST" style="display: none">
        @csrf

        <input type="text" name="password" value="" id="form-password">
    </form>

    <script>
        @if(Session::has('success'))
        Swal.fire(
            'Suecesso!',
            '{{Session::get('success')}}',
            'success'
        );
        @endif
        function deletResource(id) {
            Swal.fire({
                title: 'Tem certeza?',
                text: "O Recurso será deletado da base",
                icon: 'warning',
                showCancelButton: true,
                cancelButtonText: "Cancelar",
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Sim Deletar'
            }).then((result) => {
                if (result.value) {
                    $("#id-form-delete").attr('action', '/revendas/' + id);
                    $("#form-status").val(status);
                    $("#id-form-delete").submit();
                }
            })
        }

        function setPassword(id) {

        }
    </script>
@endsection
