@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-lg-6">
            <div class="element-wrapper">
                <div class="element-actions">
                    <a href="{{ route('revendas.index') }}" type="button" class="btn btn-outline-secondary min-width-125 pull-right">Voltar</a>
                </div>
                <h6 class="element-header">
                    Cadastro de Revendas
                </h6>
                <div class="element-content">
                    <div class="element-box">
                        {!!  Form::open(['route' => ['revendas.store'], 'enctype' => 'multipart/form-data']) !!}
                            @csrf

                            <x-legend text="Dados de Acesso"/>

                            <x-input name="name" text="Nome" placeholder="Digite o Nome" :error="$errors"/>

                            <x-input name="username" text="Username" placeholder="Digite o Username" :error="$errors"/>

                            <x-input name="password" text="Senha" placeholder="Digite a Senha" :error="$errors" />

                            <x-multiselect name="bouquet_id[]" text="Bouquets" placeholder="Selecione o(s) Bouquet(s)" :error="$errors"
                                           :options="$bouquets"/>

                            <x-legend text="Configurações"/>

                            <fieldset class="form-group">
                                <x-input name="app_name" text="Nome do App" placeholder="Digite o Nome do App" :error="$errors"/>

                                <x-input-file name="path_logo" text="Logo da Revenda" placeholder="" :error="$errors"/>

                                <x-input-file name="path_background" text="Background da Revenda" placeholder="" :error="$errors"/>

                                <div class="row">
                                    <div class="col-md-6 no-padding-content">
                                        <x-input-prepend prepend="#" value="1c4cc3" name="primary_color" text="Cor Primária" placeholder="Insira a Cor Primaria" :error="$errors"/>
                                    </div>
                                    <div class="col-md-6">
                                        <x-input-prepend prepend="#" value="1c4cc3" name="secondary_color" text="Cor Secundária" placeholder="Insira a Cor Secundária" :error="$errors"/>
                                    </div>
                                </div>

                                <x-input type="number" value="99999" name="limit_users" text="Limite de Usuários" placeholder="Digite o Limite" :error="$errors" />

                                <x-text-area name="obs" text="Observação" placeholder="Digite o texto" :error="$errors"/>

                                <x-input-check-box name="has_painel" text="Possui Painel?" :error="$errors" checked/>
                            </fieldset>

                            <x-legend text="Trial"/>

                            <fieldset class="form-group">
                                <x-input-date type="text" name="expired_at" text="Data de Expiração do Teste" placeholder="Digite o Limite" :error="$errors" />
                                <x-input-check-box name="is_trial" text="Periodo de Teste?" :error="$errors"/>
                            </fieldset>

                            <button type="submit" class="btn btn-primary min-width-125">Cadastrar</button>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
