@extends('layouts.app')

@section('content')
    <div class="row">
        <x-profile-revenda :revenda="$revenda"/>

        <div class="col-sm-7">
            <div class="element-wrapper">
                <div class="element-box">
                    {!!  Form::open(['route' => ['revendas.update', $revenda->id], 'enctype' => 'multipart/form-data']) !!}
                        @csrf
                         @method('PUT')
                        <div class="element-info">
                            <div class="element-info-with-icon">
                                <div class="element-info-icon">
                                    <div class="os-icon os-icon-airplay"></div>
                                </div>
                                <div class="element-info-text">
                                    <h5 class="element-inner-header">
                                        {{ $revenda->uuid . ' - ' .$revenda->name }}
                                    </h5>
                                </div>
                            </div>
                            <x-legend text="Dados de Acesso"/>

                            <x-input name="name" text="Nome" :value="$revenda->name" placeholder="Digite o Nome" :error="$errors"/>

                            <x-input name="username" text="Username" :value="$revenda->username" placeholder="Digite o Username" :error="$errors"/>

                            <x-multiselect name="bouquet_id[]" text="Bouquets" :value="$revenda->available_bouquets" placeholder="Selecione o(s) Bouquet(s)" :error="$errors"
                                           :options="$bouquets"/>

                            <x-legend text="Configurações"/>

                            <fieldset class="form-group">
                                <x-input name="app_name" text="Nome do App" :value="$revenda->app_name" placeholder="Digite o Nome do App" :error="$errors"/>

                                <x-input-file name="path_logo" text="Logo da Revenda" :value="$revenda->path_logo" placeholder="" :error="$errors"/>

                                <x-input-file name="path_background" text="Background da Revenda" :value="$revenda->path_background" placeholder="" :error="$errors"/>

                                <div class="row">
                                    <div class="col-md-6 no-padding-content">
                                        <x-input-prepend prepend="#" value="1c4cc3" :value="$revenda->primary_color" name="color_primary" text="Cor Primária" placeholder="Insira a Cor Primaria" :error="$errors"/>
                                    </div>
                                    <div class="col-md-6">
                                        <x-input-prepend prepend="#" value="1c4cc3" :value="$revenda->secondary_color" name="color_secondary" text="Cor Secundária" placeholder="Insira a Cor Secundária" :error="$errors"/>
                                    </div>
                                </div>

                                <x-input type="number" value="10" name="credits" :value="$revenda->credits" text="Creditos" placeholder="Digite o Limite" :error="$errors" />

                                <x-input type="number" value="99999" name="limit_users" :value="$revenda->limit_users" text="Limite de Usuários" placeholder="Digite o Limite" :error="$errors" />

                                <x-text-area name="obs" text="Observação" placeholder="Digite o texto" :value="$revenda->obs" :error="$errors"/>

                                <x-input-check-box name="has_painel" text="Possui Painel?" :error="$errors" :value="$revenda->has_painel"/>


                            </fieldset>

                            <x-legend text="Trial"/>

                            <fieldset class="form-group">
                                <x-input-date type="text" name="expired_at" text="Data de Expiração do Teste" :value="!empty($revenda->expired_at) ? $revenda->expired_at->format('d/m/Y') : ''" placeholder="Digite o Limite" :error="$errors" />
                                <x-input-check-box name="is_trial" text="Periodo de Teste?" :error="$errors" :value="$revenda->is_trial"/>
                            </fieldset>

                            <button type="submit" class="btn btn-primary min-width-125">Cadastrar</button>

                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
