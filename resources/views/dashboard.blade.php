@extends('layouts.app')

@section('content')
    <!-- Page Content -->
    <div class="content">
        <div class="row">
            <div class="col-sm-4 col-xxxl-3">
                <a class="element-box el-tablo default-color" href="#">
                    <div class="label">Total de Séries de TV</div>
                    <div class="value">{{$dash['series']}}</div>

                </a>
            </div>
            <div class="col-sm-4 col-xxxl-3 ">
                <a class="element-box el-tablo bg-olive" href="#">
                    <div class="label">Total de Filmes</div>
                    <div class="value">{{$dash['filmes']}}</div>
                </a>
            </div>
            <div class="col-sm-4 col-xxxl-3 ">
                <a class="element-box el-tablo danger-color" href="#">
                    <div class="label">Total de Streams</div>
                    <div class="value">{{$dash['streams']}}</div>
                </a>
            </div>
            <div class="col-sm-4 col-xxxl-3 ">
                <a class="element-box el-tablo success-color" href="#">
                    <div class="label">Usuários de Revenda</div>
                    <div class="value">{{$dash['user_revenda']}}</div>
                </a>
            </div>
            <div class="col-sm-4 col-xxxl-3">
                <a class="element-box el-tablo secondary-color" href="#">
                    <div class="label">Usuários Online</div>
                    <div class="value">{{$dash['user_online']}}</div>
                </a>
            </div>
            <div class="col-sm-4 col-xxxl-3">
                <a class="element-box el-tablo grey darken-2" href="#">
                    <div class="label">Usuários Admin</div>
                    <div class="value">{{$dash['user']}}</div>

                </a>
            </div>
        </div>
    </div>
    <!-- END Page Content -->
@endsection
