@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-lg-6">
            <div class="element-wrapper">
                <div class="element-actions">
                    <a href="{{ route('users.index') }}" type="button" class="btn btn-outline-secondary min-width-125 pull-right">Voltar</a>
                </div>
                <h6 class="element-header">
                    Cadastro de Usuários
                </h6>
                <div class="element-content">
                    <div class="element-box">
                        {!! Form::open(['method' => 'POST', 'action' => 'Admin\\MovieController@store', 'files' => true, 'enctype' => 'multipart/form-data']) !!}
                        @csrf


                        <x-input name="title" text="Nome do Filme" placeholder="Digite o Nome do Filme" :error="$errors"/>

                        <div class="form-group{{ $errors->has('selecturl') ? ' has-error' : '' }}">
                            {!! Form::label('selecturls', 'Add Video') !!}
                            <i class="fa fa-info-circle" data-toggle="tooltip" data-placement="top" title="Please select one of the options to add Video/Movie"></i>
                            {!! Form::select('selecturl', array('multiqcustom' => 'IFrame URL/Embed URL',
                             'youtubeapi' =>'Youtube API', 'vimeoapi' => 'Vimeo API',
                             'customurl' => 'Custom URL/ Youtube URL/ Vimeo URL'), null, ['class' => 'form-control select2','id'=>'selecturl']) !!}
                            <small class="text-danger">{{ $errors->first('selecturl') }}</small>
                        </div>

                        <x-input name="video_link" text="Link do Filme" placeholder="Digite o link do Filme" :error="$errors"/>

                        <x-input-file name="thumbnail" text="Thumbnail" placeholder="" :error="$errors"/>

                        <x-input-file name="poster" text="Poster" placeholder="" :error="$errors"/>

                        <x-text-area name="keyword" text="Meta Keyword" placeholder="Meta Keyword" :error="$errors"/>

                        <x-text-area name="description" text="Meta Descrição" placeholder="Meta Descrição" :error="$errors"/>

                        <button type="submit" class="btn btn-primary min-width-125">Cadastrar</button>

                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
