@extends('layouts.app')

@section('content')
    <x-message :message="Session::has('success') ? Session::get('success') : ''"/>

    <div class="element-wrapper">
        <div class="element-actions">
            <a href="{{ route('filmes.create') }}" type="button" class="btn btn-primary min-width-125 pull-right">Cadastrar Filme</a>
        </div>
        <h6 class="element-header">
            Gerenciar Filmes
        </h6>

        <div class="element-content">
            <table id="dataTable1" class="table table-bordered table-striped table-vcenter js-dataTable-full">
                <thead>
                <tr>
                    <th class="text-center"></th>
                    <th class="text-center" style="width: 5%">Thumbnail</th>
                    <th>Titulo</th>
                    <th class="d-none d-sm-table-cell">Status</th>
                    <th class="text-center" style="width: 15%;"></th>
                </tr>
                </thead>
                <tbody>
                    @foreach($movies as $movie)
                        <tr>
                            <td class="text-center">{{ $movie->id }}</td>
                            <td class="text-center">
                                <x-image-cell :url="$movie->thumbnail"/>
                            </td>
                            <td class="text-center">{{ $movie->title }}</td>
                            <td class="text-center">{{ $movie->status }}</td>
                            <td class="text-center">
                                <x-button :route="route('filmes.edit', $movie->id)" type="info" icon="os-icon-edit-32" tooltip="Editar Filme" />
                                <x-button :route="route('filmes.edit', $movie->id)" type="warning" icon="os-icon-dollar-sign" tooltip="Liberar Filme" />
                                <x-button :route="route('filmes.edit', $movie->id)" type="primary" icon="os-icon-ui-09" tooltip="Player" />
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>


@endsection
