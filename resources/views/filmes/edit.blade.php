@extends('layouts.app')

@section('content')
    <x-message :message="Session::has('success') ? Session::get('success') : ''"/>

    <div class="row">
        <div class="col-lg-12">
            <div class="element-wrapper">
                <div class="element-actions">
                    <a href="{{ route('users.index') }}" type="button" class="btn btn-outline-secondary min-width-125 pull-right">Voltar</a>
                </div>
                <h6 class="element-header">
                    Edição de Filmes
                </h6>

                <div class="element-content">
                    <div class="element-box">
                        <form>
                            <div class="steps-w">
                                <div class="step-triggers">
                                    <a class="step-trigger active" href="#stepContent1">Detalhes</a>
                                    <a class="step-trigger" href="#stepContent2">Informações</a>
                                </div>
                                <div class="step-contents">
                                    <div class="step-content active" id="stepContent1">
                                        <div class="row">

                                        </div>
                                        <div class="row">

                                        </div>
                                        <div class="form-group">
                                            <label> Example textarea</label><textarea class="form-control" rows="3"></textarea>
                                        </div>
                                        <div class="form-buttons-w text-right">
                                            <a class="btn btn-primary step-trigger-btn" href="#stepContent2"> Continue</a>
                                        </div>
                                    </div>
                                    <div class="step-content" id="stepContent2">
                                        <div class="form-group">
                                            <label for=""> Email address</label><input class="form-control" placeholder="Enter email" type="email">
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for=""> Password</label><input class="form-control" placeholder="Password" type="password">
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="">Confirm Password</label><input class="form-control" placeholder="Password" type="password">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for=""> Regular select</label><select class="form-control">
                                                <option>
                                                    Select State
                                                </option>
                                                <option>
                                                    New York
                                                </option>
                                                <option>
                                                    California
                                                </option>
                                                <option>
                                                    Boston
                                                </option>
                                                <option>
                                                    Texas
                                                </option>
                                                <option>
                                                    Colorado
                                                </option>
                                            </select>
                                        </div>
                                        <div class="form-buttons-w text-right">
                                            <a class="btn btn-primary step-trigger-btn" href="#stepContent3"> finaliza</a>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
