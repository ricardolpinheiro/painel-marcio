<a href="{{ $route }}" id="{{$id}}" type="button" data-id="{{$value}}" class="btn btn-{{ $type }}" data-toggle="tooltip" title="{{ $tooltip }}">
    <i class="os-icon {{ $icon }}"></i>
</a>
