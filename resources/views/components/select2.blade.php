<div class="form-group {{ ($hasError == true) ? "has-error has-danger" : '' }} ">
    <label for="{{ $name }}">{{ __(ucfirst($text ?? $name)) }}</label>
    {!! Form::select($name, $options, null, ['class' => 'form-control select2','id'=>$name]) !!}
    @if($hasError)
        <div class="help-block form-text text-muted form-control-feedback">{{ $message }}</div>
    @endif
</div>


