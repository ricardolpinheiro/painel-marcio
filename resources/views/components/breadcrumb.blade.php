<ul class="breadcrumb">
    @foreach($breadcrumb as $path)
        <li class="breadcrumb-item">
            <a href="index.html">{{ ucfirst($path) }}</a>
        </li>
    @endforeach
</ul>
