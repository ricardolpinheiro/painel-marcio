<div class="form-group {{ ($hasError == true) ? "has-error has-danger" : '' }}">
<label for="{{ $id }}">{{ __(ucfirst($text ?? $name)) }}</label>

    <select class="form-control select2" id="{{ $id }}" name="{{ $name }}" multiple="multiple">
        @foreach($options as $key => $opt)

            @if(in_array($key, $value))
                <option value="{{$key}}" selected>{{$opt}}</option>
            @else
                <option value="{{$key}}">{{$opt}}</option>
            @endif

        @endforeach
    </select>


    @if($hasError)
        <div class="help-block form-text text-muted form-control-feedback">{{ $message }}</div>
    @endif
</div>
