
<div class="form-check {{ ($hasError == true) ? "has-error has-danger" : '' }}">
    <label for="{{ $name }}">

      <input class="form-check-input" type="checkbox" name="{{ $name }}" id="{{ $name }}"{{ ($checked === true) ? 'checked' : '' }}>
        {{ __(ucfirst($text ?? $name)) }}
    </label>
</div>
