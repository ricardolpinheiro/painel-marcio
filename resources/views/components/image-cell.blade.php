<div class="cell-image-list">
    <a href="{{ $url }}" target="_blank">
        <div class="cell-img" style="background-image: url({{$url ?? './img/default.jpg'}});background-size: cover;" ></div>
    </a>
</div>
