<div class="col-sm-5">
    <div class="user-profile compact">
        <div class="up-head-w" style="background-image:url({{$revenda->path_background ?? $revenda->path_logo}})">
            <div class="up-main-info">
                <h2 class="up-header">
                    {{ $revenda->name }}
                </h2>
            </div>
            <svg class="decor" width="842px" height="219px" viewBox="0 0 842 219" preserveAspectRatio="xMaxYMax meet" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><g transform="translate(-381.000000, -362.000000)" fill="#FFFFFF"><path class="decor-path" d="M1223,362 L1223,581 L381,581 C868.912802,575.666667 1149.57947,502.666667 1223,362 Z"></path></g></svg>
        </div>
        <div class="up-controls">
            <div class="row">
                <div class="col-sm-6">
                    <div class="value-pair">
                        <div class="label">
                            Status:
                        </div>
                        @if($revenda->status == 1)
                            <div class="value badge badge-pill badge-success">
                                Ativo
                            </div>
                        @else
                            <div class="value badge badge-pill badge-danger">
                                Inativo
                            </div>
                        @endif
                    </div>
                </div>
                <div class="col-sm-6 text-right"><a class="btn btn-primary btn-sm" href=""><i class="os-icon os-icon-link-3"></i><span>Visitar Painel</span></a></div>
            </div>
        </div>
        <div class="up-contents">
            <div class="m-b">
                <div class="row m-b">
                    <div class="col-sm-6 b-r b-b">
                        <div class="el-tablo centered padded-v">
                            <div class="value">
                                {{$revenda->users->count()}}
                            </div>
                            <div class="label">
                                Usuários Cadastrados
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 b-b">
                        <div class="el-tablo centered padded-v">
                            <div class="value">
                                {{$revenda->credits}}
                            </div>
                            <div class="label">
                                Creditos
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
