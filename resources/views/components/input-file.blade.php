<div class="form-group {!! $display !!} {{ ($hasError == true) ? "has-error has-danger" : '' }}">
    <label for="{{ $name }}">{{ __(ucfirst($text ?? $name)) }}</label>
    <input type="file" class="form-control {!! $display !!}" id="{{ $name }}" name="{{ $name }}" placeholder="{{ $placeholder }}" @if($required) required @endif>

    @if(!empty($icon))
        <div class="pre-icon os-icon {{ $icon }}"></div>
    @endif
    @if($hasError)
        <div class="help-block form-text text-muted form-control-feedback">{{ $message }}</div>
    @endif
</div>
