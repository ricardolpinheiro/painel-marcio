<div class="menu-w selected-menu-color-light menu-activated-on-hover menu-has-selected-link color-scheme-dark color-style-bright sub-menu-color-bright menu-position-side menu-side-left menu-layout-compact sub-menu-style-over">
    <div class="logo-w text-center" style="display: block;">
        <a class="logo text-center" href="#">

            <div class="logo-label text-center">
                Painel Admin
            </div>
        </a>
    </div>
    <div class="logged-user-w avatar-inline">
        <div class="logged-user-i">
            <div class="avatar-w">
                <img alt="" src="{{ url('img/profile.jpg') }}">
            </div>
            <div class="logged-user-info-w">
                <div class="logged-user-name">
                    {{ Auth::user()->name }}
                </div>
                <div class="logged-user-role">
                    Administrator
                </div>
            </div>
            <div class="logged-user-toggler-arrow">
                <div class="os-icon os-icon-chevron-down"></div>
            </div>
        </div>
    </div>

    <ul class="main-menu">
        <li class="sub-header">
            <span>Dashboard</span>
        </li>
        <li class="selected">
            <a href="{{ route('home') }}">
                <div class="icon-w">
                    <div class="os-icon os-icon-layout"></div>
                </div>
                <span>Inicio</span>
            </a>
        </li>
        <li class="sub-header">
            <span>Cadastros</span>
        </li>
        <li class="selected">
            <a href="{{ route('users.index') }}">
                <div class="icon-w">
                    <div class="os-icon os-icon-user"></div>
                </div>
                <span>Usuários</span>
            </a>
        </li>
        <li>
            <a href="{{ route('revendas.index') }}">
                <div class="icon-w">
                    <div class="os-icon os-icon-tv"></div>
                </div>
                <span>Revendas</span>
            </a>
        </li>
        <li>
            <a href="{{ route('bouquets.index') }}">
                <div class="icon-w">
                    <div class="os-icon os-icon-layout"></div>
                </div>
                <span>Bouquets</span>
            </a>
        </li>
        <li>
            <a href="{{ route('categories.index') }}">
                <div class="icon-w">
                    <div class="os-icon os-icon-tag"></div>
                </div>
                <span>Categories de Stream</span>
            </a>
        </li>
        <li>
            <a href="{{ route('hosts.index') }}">
                <div class="icon-w">
                    <div class="os-icon os-icon-box"></div>
                </div>
                <span>Host</span>
            </a>
        </li>

        <li class="sub-header">
            <span>Pré Cadastro</span>
        </li>
        <li>
            <a href="{{ route('series.index') }}">
                <div class="icon-w">
                    <div class="os-icon os-icon-layout"></div>
                </div>
                <span>Séries</span>
            </a>
        </li>
        <li>
            <a href="{{ route('seasons.index') }}">
                <div class="icon-w">
                    <div class="os-icon os-icon-layout"></div>
                </div>
                <span>Temporadas</span>
            </a>
        </li>
        <li>
            <a href="{{ route('collections.index') }}">
                <div class="icon-w">
                    <div class="os-icon os-icon-layout"></div>
                </div>
                <span>Coleções</span>
            </a>
        </li>
        <li class="sub-header">
            <span>Midias</span>
        </li>
        <li>
            <a href="{{ route('streams.index') }}">
                <div class="icon-w">
                    <div class="os-icon os-icon-old-tv-2"></div>
                </div>
                <span>Streams</span>
            </a>
        </li>
        <li>
            <a href="{{ route('movies.index') }}">
                <div class="icon-w">
                    <div class="os-icon os-icon-film"></div>
                </div>
                <span>Filmes</span>
            </a>
        </li>
        <li>
            <a href="{{ route('episodes.index') }}">
                <div class="icon-w">
                    <div class="os-icon os-icon-layout"></div>
                </div>
                <span>Series TV</span>
            </a>
        </li>
        <li>
            <a href="{{ route('radios.index') }}">
                <div class="icon-w">
                    <div class="os-icon os-icon-mic"></div>
                </div>
                <span>Radios</span>
            </a>
        </li>
        <li class="sub-header">
            <span>Geral</span>
        </li>
        <li>
            <a href="{{ route('epg.index') }}">
                <div class="icon-w">
                    <div class="os-icon os-icon-layout"></div>
                </div>
                <span>EPG</span>
            </a>
        </li>
        <li>
            <a href="#">
                <div class="icon-w">
                    <div class="os-icon os-icon-layout"></div>
                </div>
                <span>Tickets</span>
            </a>
        </li>
        <li>
            <a href="{{ url('/log-viewer') }}">
                <div class="icon-w">
                    <div class="os-icon os-icon-book"></div>
                </div>
                <span>Log</span>
            </a>
        </li>
        <li>
            <a href="{{ url('/audits') }}">
                <div class="icon-w">
                    <div class="os-icon os-icon-book"></div>
                </div>
                <span>Auditoria</span>
            </a>
        </li>
    </ul>

    </div>
