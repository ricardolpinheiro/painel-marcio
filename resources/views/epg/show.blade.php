@extends('layouts.app')

@section('content')
    <x-message :message="Session::has('success') ? Session::get('success') : ''"/>

    <div class="element-wrapper">
        <h6 class="element-header">
            Listagem Epg
        </h6>

        <div class="element-content">
            <table id="dataTable1" class="table table-bordered table-striped table-vcenter js-dataTable-full">
                <thead>
                <tr>
                    <th class="text-center"></th>
                    <th class="text-center">Titulo</th>
                    <th>Programa</th>
                    <th class="d-none d-sm-table-cell">Inicio</th>
                    <th class="d-none d-sm-table-cell">Fim</th>
                </tr>
                </thead>
                <tbody>
                    @foreach($progs as $prog)
                        <tr>
                            <td class="text-center">{{ $prog->id }}</td>
                            <td class="text-center">{{ $prog->titulo }}</td>

                            <td class="text-center">{{ $prog->id_programa }}</td>
                            <td class="text-center">{{ $prog->dh_inicio->format('H:i:s') }}</td>
                            <td class="text-center">{{ $prog->dh_fim->format('H:i:s') }}</td>

                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>


@endsection
