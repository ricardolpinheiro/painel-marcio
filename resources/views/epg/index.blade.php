@extends('layouts.app')

@section('content')
    <x-message :message="Session::has('success') ? Session::get('success') : ''"/>

    <div class="element-wrapper">
        <h6 class="element-header">
            Listagem Epg
        </h6>

        <div class="element-content">
            <table id="dataTable1" class="table table-bordered table-striped table-vcenter js-dataTable-full">
                <thead>
                <tr>
                    <th class="text-center"></th>
                    <th class="text-center">Logo</th>
                    <th>Nome</th>
                    <th class="d-none d-sm-table-cell">Id Canal</th>
                    <th class="text-center" style="width: 15%;"></th>
                </tr>
                </thead>
                <tbody>
                    @foreach($canais as $canal)
                        <tr>
                            <td class="text-center">{{ $canal->id }}</td>
                            <td class="text-center">
                                <x-image-cell :url="$canal->url_imagem"/>
                            </td>
                            <td class="text-center">{{ $canal->nome }}</td>
                            <td class="text-center">{{ $canal->id_canal }}</td>
                            <td class="text-center">
                                <x-button :route="route('epg.show', $canal->id)" type="success" icon="os-icon-edit-32" tooltip="Visualizar Programação" />
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>


@endsection
