@extends('layouts.app')

@section('content')
    <x-message :message="Session::has('success') ? Session::get('success') : ''"/>

    <div class="row">
        <div class="col-lg-6">
            <div class="element-wrapper">
                <div class="element-actions">
                    <a href="{{ route('radios.index') }}" type="button" class="btn btn-outline-secondary min-width-125 pull-right">Voltar</a>
                </div>
                <h6 class="element-header">
                    Edição de Radio
                </h6>

                <div class="element-content">
                    <div class="element-box">
                        {!!  Form::model($stream, ['method' => 'PUT','route' => ['radios.update', $radio->id],  'enctype' => 'multipart/form-data']) !!}
                        @csrf

                        @csrf
                        @method('PUT')

                        <x-input name="name" text="Nome" :value="$stream->name" placeholder="Digite o Nome da Stream" :error="$errors"/>

                        <x-input-prepend prepend="http://" :value="$stream->stream_link" name="stream_link" text="Stream URL" placeholder="Insira a URL da Stream" :error="$errors"/>

                        <x-select2 name="category_id" text="Categoria" :value="$stream->category_id" placeholder="Selecione a Categoria" :error="$errors"
                                   :options="$categories"/>

                        <x-multiselect name="bouquet_id[]" text="Bouquets" :value="$stream->available_bouquets" placeholder="Selecione o(s) Bouquet(s)" :error="$errors"
                                       :options="$bouquets"/>

                        <x-input-file name="stream_logo" text="Stream Logo" placeholder="" :error="$errors"/>

                        <x-text-area name="notes" text="Observação" :value="$stream->notes" placeholder="Digite o texto" :error="$errors"/>

                        <button type="submit" class="btn btn-primary min-width-125">Salvar</button>

                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


