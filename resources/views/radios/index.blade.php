@extends('layouts.app')

@section('content')
    <x-message :message="Session::has('success') ? Session::get('success') : ''"/>

    <div class="element-wrapper">
        <div class="element-actions">
            <a href="{{ route('radios.create') }}" type="button" class="btn btn-primary min-width-125 pull-right">Cadastrar Radio</a>
        </div>
        <h6 class="element-header">
            Gerenciar Radio
        </h6>

        <div class="element-content">
            <table id="dataTable1" class="table table-bordered table-striped table-vcenter js-dataTable-full">
                <thead>
                <tr>
                    <th class="text-center"></th>
                    <th class="text-center">Logo</th>
                    <th class="text-center">Nome</th>
                    <th class="d-none d-sm-table-cell" style="width: 15%;">Status</th>
                    <th class="text-center" style="width: 15%;"></th>
                </tr>
                </thead>
                <tbody>
                    @foreach($radios as $radio)
                        <tr>
                            <td class="text-center" style="width: 5%">{{ $radio->id }}</td>
                            <td class="text-center" style="width: 5%">
                                <x-image-cell :url="$radio->stream_logo"/>
                            </td>
                            <td class="text-center">{{ $radio->name }}</td>
                            <td class="text-center">
                                <x-status-table :status="$radio->status"/>
                            </td>
                            <td class="text-center">
                                <x-button :route="route('streams.edit', $radio->id)" type="info" icon="os-icon-edit-32" tooltip="Editar Bouquet" />
                                <button type="button" class="btn btn-danger" data-toggle="tooltip" title="Deletar" onclick="deletResource({{$radio->id}})">
                                    <i class="os-icon os-icon-trash-2"></i>
                                </button>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>


@endsection


@section('after-script')

    <form action="" id="id-form-delete" method="POST" style="display: none">
        @method('DELETE')
        @csrf
    </form>

    <script>
        @if(Session::has('success'))
        Swal.fire(
            'Suecesso!',
            '{{Session::get('success')}}',
            'success'
        );
        @endif
        function deletResource(id) {
            Swal.fire({
                title: 'Tem certeza?',
                text: "O Recurso será deletado da base",
                icon: 'warning',
                showCancelButton: true,
                cancelButtonText: "Cancelar",
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Sim Deletar'
            }).then((result) => {
                if (result.value) {
                    $("#id-form-delete").attr('action', '/radios/' + id);
                    $("#form-status").val(status);
                    $("#id-form-delete").submit();
                }
            })
        }
    </script>
@endsection
