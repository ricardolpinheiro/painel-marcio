@extends('layouts.app')

@section('content')
    <x-message :message="Session::has('success') ? Session::get('success') : ''"/>

    <div class="row">
        <div class="col-lg-6">
            <div class="element-wrapper">
                <div class="element-actions">
                    <a href="{{ route('hosts.index') }}" type="button" class="btn btn-outline-secondary min-width-125 pull-right">Voltar</a>
                </div>
                <h6 class="element-header">
                    Edição de hosts
                </h6>

                <div class="element-content">
                    <div class="element-box">
                        {!!  Form::model($host, ['method' => 'PUT','route' => ['hosts.update', $host->id]]) !!}
                        @csrf

                        <x-input name="host_name" text="Nome" :value="$host->host_name" placeholder="Digite o Nome" :error="$errors"/>

                        <button type="submit" class="btn btn-primary min-width-125">Salvar</button>

                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
