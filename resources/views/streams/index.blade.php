@extends('layouts.app')

@section('content')
    <x-message :message="Session::has('success') ? Session::get('success') : ''"/>

    <div class="element-wrapper">
        <div class="element-actions">
            <a href="{{ route('streams.create') }}" type="button" class="btn btn-primary min-width-125 pull-right">Cadastrar Streams</a>
        </div>
        <h6 class="element-header">
            Gerenciar Streams
        </h6>

        <div class="element-content">
            <table id="dataTable1" class="table table-bordered table-striped table-vcenter js-dataTable-full">
                <thead>
                <tr>
                    <th class="text-center"></th>
                    <th class="text-center">Logo</th>
                    <th class="text-center">Nome</th>
                    <th class="text-center">Host Name</th>
                    <th class="text-center">EPG</th>
                    <th class="d-none d-sm-table-cell" style="width: 5%;">Status</th>
                    <th class="text-center" style="width: 15%;"></th>
                </tr>
                </thead>
                <tbody>
                    @foreach($streams as $stream)
                        <tr>
                            <td class="text-center" style="width: 5%">{{ $stream->id }}</td>
                            <td class="text-center" style="width: 5%">
                                <x-image-cell :url="$stream->stream_logo"/>
                            </td>
                            <td class="text-center">{{ $stream->name }}</td>
                            <td class="text-center">{{ $stream->host->host_name ?? 'N/A' }}</td>
                            <td class="text-center">{{ $stream->epg->id_canal  ?? 'N/A' }}</td>
                            <td class="text-center">
                                <x-status-table :status="$stream->status"/>
                            </td>
                            <td class="text-center">
                                <x-button :route="route('streams.edit', $stream->id)" type="info" icon="os-icon-edit-32" tooltip="Editar Bouquet" />
                                <x-button :route="route('streams.edit', $stream->id)" type="danger" icon="os-icon-close" tooltip="Deletar Bouquet"/>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>


@endsection
