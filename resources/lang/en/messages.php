<?php


return [
    'users' => [
        'created' => 'Usuário Cadastrado com Sucesso',
        'updated' => 'Usuário atualizado'
    ],
    'streams' => [
        'created' => 'Stream Cadastrado com Sucesso',
        'updated' => 'Stream atualizado'
    ],

];
