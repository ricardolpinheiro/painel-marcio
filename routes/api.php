<?php

use App\Movie;
use App\Stream;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/sync', function (Request $request) {

    $movie = Movie::all()->take(50);

    $stream = Stream::all()->take(50);

    $mmovie = DB::select("SELECT b.id, b.name FROM bouquet_relations br
                        inner join bouquets b on b.id = br.resource_id
                        where br.resource = 'movie' and b.status in (1, '1');");

    $mstream = DB::select("SELECT b.id, b.name FROM bouquet_relations br
                        inner join bouquets b on b.id = br.resource_id
                        where br.resource = 'stream' and b.status in (1, '1');");

    $return = ['response' => ['menu_movie' => $mmovie, 'menu_stream' => $mstream, 'movie' => $movie, 'stream' => $stream ]];

    return response()->json($return);
});

