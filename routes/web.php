<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/




Auth::routes();


Route::group(['middleware' => ['auth']], function () {
    Route::get('/', 'HomeController@index')->name('home');
    Route::resource('users', 'Admin\\UserController');
    Route::resource('revendas', 'Admin\\RevendaController');
    Route::resource('bouquets', 'Admin\\BouquetController');
    Route::resource('categories', 'Admin\\CategoryController');
    Route::resource('streams', 'Admin\\StreamController');
    Route::resource('radios', 'Admin\\RadioController');
    Route::resource('epg', 'Admin\\EpgController');
    Route::resource('hosts', 'Admin\\HostController');
    Route::resource('movies', 'Admin\\MovieController');
    Route::resource('collections', 'Admin\\CollectionController');
    Route::resource('series', 'Admin\\SerieController');
    Route::resource('seasons', 'Admin\\SeasonController');
    Route::resource('episodes', 'Admin\\EpisodeController');

    Route::get('audits', 'AuditController@index');
    Route::post('/revendas/password/?', 'Admin\\RevendaController@password');
});

Route::get('job', 'HomeController@job');
Route::get('card', 'HomeController@card');
Route::get('csv', 'HomeController@csv');
Route::get('screen', 'HomeController@homescreen');
Route::get('teste', 'HomeController@teste');

//Route::get('/home', 'HomeController@index')->name('home');
//Route::get('/dashboard', 'HomeController@dashboard')->name('dashboard');
