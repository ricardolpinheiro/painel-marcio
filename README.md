
## Install node

curl -sL https://deb.nodesource.com/setup_13.x | sudo -E bash -
sudo apt-get install -y nodejs

## Install Required Packages
sudo apt update
sudo apt install composer
sudo apt-get install php7.2-curl
sudo apt-get install php-gd php-xml php7.2-mbstring
sudo apt install zip

##Import Repo
cd /var/www
git pull origin master

##Enable CHMOD
chmod -R 777 ./storage
chmod -R 777 ./public
chmod -R 777 ./bootstrap
sudo a2enmod rewrite

##Install Dependencies
sudo composer install --ignore-platform-reqs
npm install

##Build CSS/JS
npm run prod

#Copy Config File
cp .env.production .env

##Migration
php artisan migrate
php artisan db:seed

##Import Data
php artisan import:data
php artisan import:sync

##Atualizar Arquivos Apache

##Reiniciar Apache
service apache2 restart
