<?php

namespace App\Http\Controllers;

use App\Jobs\SyncTMDBMovie;
use App\Radio;
use App\TvSerieSeason;
use App\Util\CsvImporter;
use App\Jobs\EpgChannel;
use App\Jobs\EpgChannelItem;
use App\Movie;
use App\Stream;
use App\TvSerie;
use App\User;
use App\UserRevenda;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use \Gumlet\ImageResize;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $dash['series'] = TvSerie::all()->count();
        $dash['filmes'] = Movie::all()->count();
        $dash['streams'] = Stream::all()->count();
        $dash['user_revenda'] = UserRevenda::all()->count();
        $dash['user_online'] = 0;
        $dash['user'] = User::all()->count();


        return view('dashboard')->with(compact('dash'));
    }

    public function dashboard()
    {
        return view('dashboard');
    }

    public function homescreen() {
        $movies = Movie::where('home_screen', "!=", null)->get();
        return response()->json($movies);
    }

    public function teste() {

        var_dump(storage_path('movie/1B8pVr4bxhA8cHyv4drb6cSqghk'));
        $image = new ImageResize(storage_path('app/public/movie/1B8pVr4bxhA8cHyv4drb6cSqghk.jpg'));
        $image->resizeToWidth(300);
        $image->save(public_path('image2.jpg'));

        return response()->json(['teste' => "teste"]);
    }

    public function csv() {
//        //TODO temporaário para importação de dados
//        set_time_limit(300);
//        if(env('APP_ENV') === 'local') {
//            ini_set('auto_detect_line_endings',TRUE);
//            ini_set('max_execution_time ',1000000);
//            $handle = fopen(public_path('database.csv'),'r');
//            $importer = new CsvImporter(public_path('database.csv'),true);
//
//            foreach ($importer->get() as $key => $value) {
//                $chave = explode(",", array_key_first($value));
//                $data = explode(",", $value[array_key_first($value)]);
//                print_r("Importando: " . str_replace("\"", "", $data[1]) . "-" . $key . PHP_EOL );
//                if(str_replace("\"", "", $data[1]) === "Live Streams") {
//
//                    $stream=[];
//                    $stream['name'] = trim(str_replace("\"", "", $data[2]));
//                    $stream['stream_logo'] = str_replace("\"", "", $data[4]);
//                    $stream['stream_link'] = str_replace("\"", "", str_replace("\\", "",
//                        str_replace('"]"', "", str_replace('"["', "", $data[3]))));
//
//                    Stream::create($stream);
//                    unset($stream);
//                }
//
//                if (str_replace("\"", "", $data[1]) === "Movies") {
//                    $movie=[];
//
//                    if(substr($data[6], 1, 7) === "tmdb_id") {
//                        $id = str_replace("\"", "", substr($data[6], 10, strlen($data[6])));
//                        if(!empty($id)) {
//                            $movie['tmdb_id'] = $id;
//                        }
//                    }
//
//                    $movie['title'] = str_replace("\"", "", $data[2]);
//                    $movie['movie_link'] = "http://51.79.82.9:25461/movie/ricpeludo/288299/"
//                        . str_replace("\"", "", $data[0])
//                        . substr(substr($data[3], (strlen($data[3])-7), strlen($data[3])), 0, 4);
//
//                    $movie['category_id'] = 1;
//                    Movie::create($movie);
//                    unset($movie);
//                }
//
//                if (str_replace("\"", "", $data[1]) === "Radio") {
//                    $radio = [];
//                    $radio['name'] = str_replace("\"", "", $data[2]);
//                    $radio['stream_logo'] = $data[4];
//                    $radio['category_id'] = 1;
//                    $radio['stream_link'] = str_replace("\"", "", str_replace("\\", "",
//                        str_replace('"]"', "", str_replace('"["', "", $data[3]))));
//
//                    Radio::create($radio);
//                    unset($radio);
//                }
//
//                if (str_replace("\"", "", $data[1]) === "TV Series") {
//                    $serie=[];
//                    $dados = explode("-", str_replace("\"", "", $data[2]));
//                    if(count($dados) == 3) {
//                        $dados[1] = str_replace(" ", "", $dados[1]);
//
//                        $serie['name'] = trim($dados[0]);
//                        $serie['episodio'] = substr($dados[1], 4, 2);
//                        $serie['temporada'] = substr($dados[1], 1, 2);
//                        $serie['link'] = str_replace("\"", "", str_replace("\\", "",
//                            str_replace('"]"', "", str_replace('"["', "", $data[3]))));
//
//
//                        $seriedb = checkExistsSerie($serie);
//
//                        if($seriedb instanceof TvSerie) {
//
//                            $seasondb = checkExistsSerieSeason($seriedb, $serie);
//                            if($seasondb instanceof TvSerieSeason) {
//                                $episode = checkExistsSerieSeasonepisode($seasondb, $serie);
//                            } else {
//                                Log::info('Não achou: Season - ' . $serie['name'] . '-'.$serie['Temporada']);
//                            }
//
//                        } else {
//                            Log::info('Não achou: Série - ' . $serie['name']);
//                        }
//
//                    };
//
//                }
//            }
//        }
    }

    public function card() {
        $movies = Movie::where('released', 'like', '%2019%')
            ->where('thumbnail', '!=', null)
            ->where('poster', '!=', null)
            ->where('home_screen', '=', null)
            ->orderBy('id', 'desc')
            ->take(1)
            ->get();

        $movie = $movies[0];
//        $html = "<div class=\"movie_card\" id=\"ave\"> <div class=\"blur_back ave_back\"></div><div class=\"info_section\"> <div class=\"movie_header\"> <h1>Capitão América: O Primeiro Vingador</h1> <h4>2018, Ryan Coogler</h4> <span class=\"minutes\">134 min</span> <p class=\"type\">Action, Adventure, Sci-Fi</p></div><div class=\"movie_desc\"> <p class=\"text\"> T'Challa, the King of Wakanda, rises to the throne in the isolated, technologically advanced African nation, but his claim is challenged by a vengeful outsider who was a childhood victim of T'Challa's father's mistake. </p></div></div></div>";
//        $css = " @import url('https://fonts.googleapis.com/css?family=Montserrat:300,400,700,800'); *{box-sizing: border-box; margin: 0;}html, body{margin: 0; font-family: 'Montserrat', helvetica, arial, sans-serif; font-size: 14px; font-weight: 400;}.movie_card{position: relative; display: block; width: 640px; height: 350px; overflow: hidden; border-radius: 0; -webkit-transition: all 0.4s; transition: all 0.4s;}.movie_card:hover{-webkit-transform: scale(1.02); transform: scale(1.02); -webkit-transition: all 0.4s; transition: all 0.4s;}.movie_card .info_section{position: relative; width: 90%; height: 100%; background-blend-mode: multiply; z-index: 2; border-radius: 0; margin-left: 25%;}.movie_card .info_section .movie_header{position: relative; padding: 25px; height: 40%; margin-left: 10%;}.movie_card .info_section .movie_header h1{color: #fff; font-weight: 400;}.movie_card .info_section .movie_header h4{color: #9ac7fa; font-weight: 400;}.movie_card .info_section .movie_header .minutes{display: inline-block; margin-top: 10px; color: #fff; padding: 5px; border-radius: 0; border: 1px solid rgba(255, 255, 255, 0.13);}.movie_card .info_section .movie_header .type{display: inline-block; color: #cee4fd; margin-left: 10px;}.movie_card .info_section .movie_header .locandina{position: relative; float: left; margin-right: 20px; height: 120px; box-shadow: 0 0 20px -10px rgba(0, 0, 0, 0.5);}.movie_card .info_section .movie_desc{padding: 25px; height: 50%; margin-left: 10%;}.movie_card .info_section .movie_desc .text{color: #cfd6e1;}.movie_card .info_section .movie_social{height: 10%; padding-left: 15px; padding-bottom: 20px;}.movie_card .info_section .movie_social ul{list-style: none; padding: 0;}.movie_card .info_section .movie_social ul li{display: inline-block; color: rgba(255, 255, 255, 0.4); -webkit-transition: color 0.3s; transition: color 0.3s; -webkit-transition-delay: 0.15s; transition-delay: 0.15s; margin: 0 10px;}.movie_card .info_section .movie_social ul li:hover{-webkit-transition: color 0.3s; transition: color 0.3s; color: rgba(255, 255, 255, 0.8);}.movie_card .info_section .movie_social ul li i{font-size: 19px; cursor: pointer;}.movie_card .blur_back{position: absolute; top: 0; z-index: 1; height: 100%; left: 0; background-size: cover; border-radius: 0;}@media screen and (min-width: 768px){.movie_header{width: 70%;}.movie_desc{width: 70%;}.info_section{background: -webkit-gradient(linear, left top, right top, color-stop(50%, #0d0d0c), to(transparent)); background: linear-gradient(to left, #0d0d0c 90%, transparent 100%);}.blur_back{width: 35%; background-position: 100% 10% !important;}}@media screen and (max-width: 768px){.movie_card{width: 95%; margin: 70px auto; min-height: 350px; height: auto;}.blur_back{width: 100%; background-position: 50% 50% !important;}.movie_header{width: 100%; margin-top: 85px;}.movie_desc{width: 100%;}.info_section{background: -webkit-gradient(linear, left bottom, left top, color-stop(50%, #141413), to(transparent)); background: linear-gradient(to top, #141413 50%, transparent 100%); display: inline-grid;}}.ave_back{background: url('https://image.tmdb.org/t/p/w533_and_h300_bestv2/uZNhVUocfi44FuyR3FmHOFLwio4.jpg'); background-size: cover !important; background-position: center center !important;}";
//
//        $google_fonts = "Roboto";
//
//        $data = array('html'=>$html,
//            'css'=>$css,
//            'google_fonts'=>$google_fonts);
//
//        $ch = curl_init();
//
//        curl_setopt($ch, CURLOPT_URL, "https://hcti.io/v1/image");
//        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
//
//        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
//
//        curl_setopt($ch, CURLOPT_POST, 1);
//// Retrieve your user_id and api_key from https://htmlcsstoimage.com/dashboard
//        curl_setopt($ch, CURLOPT_USERPWD, "5cd40988-1214-43f0-be10-0d9b3ae7651e" . ":" . "95677cef-e223-42b3-8151-ecf77a4174bb");
//
//        $headers = array();
//        $headers[] = "Content-Type: application/x-www-form-urlencoded";
//        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
//
//        $result = curl_exec($ch);
//        if (curl_errno($ch)) {
//            echo 'Error:' . curl_error($ch);
//        }
//        curl_close ($ch);
//        $res = json_decode($result,true);
//        var_dump($res);
//        echo $res['url'];
        return view('card')->with(compact('movie'));
    }
}
