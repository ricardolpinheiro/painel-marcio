<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Movie;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{

    public function me()
    {
        $user = User::find(Auth::user()->id);
        return view('users.profile')->with(compact('user'));
    }

    public function show($id){
        return   abort(404, 'Page not found');
    }

    public function index()
    {
        $users = User::all();
        return view('users.index')->with(compact('users'));
    }

    public function create()
    {
        $users = User::all();
        return view('users.create')->with(compact('users'));
    }

    public function store(Request $request) {

        $request->validate([
            'name' => 'required',
            'username' => 'required|unique:users,username',
            'password' => 'required|min:4',
        ]);

        $data = $request->all();

        $data['is_admin'] = true;
        $data['status'] = '1';

        $user = User::create($data);
        return redirect()->route('users.index')->with('success', __('messages.users.created'));
    }

    public function edit($id)
    {
        $status = ["" => "Selecione", "0" => "Inativo", "1" => "Ativo"];
        $user = User::findOrFail($id);
        return view('users.edit')->with(compact('user', 'status'));
    }

    public function update(Request $request, $id) {

        $user = User::find($id);
        $data = $request->all();

        if($user->username !== $data['username']) {
            $request->validate([
                'name' => 'required',
                'username' => 'required|unique:users,username',
            ]);
        } else {
            $request->validate([
                'name' => 'required',
                'username' => 'required',
            ]);
        }

        $data['is_admin'] = true;

        $user->update($data);
        $user->save();
        return redirect()->route('users.index')->with('success', __('messages.users.updated'));
    }

    public function destroy($id) {
        $model = User::findOrFail($id);

        if($model) {
            $model->deleted_by = Auth::user()->id;
            $model->delete();
            $model->save();
            return redirect()->route('users.index')->with('success', __('messages.users.deleted'));
        }
        return redirect()->route('users.index')->with('error', __('messages.users.not_found'));

    }
}
