<?php

namespace App\Http\Controllers\Admin;

use App\Bouquet;
use App\Category;
use App\Host;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HostController extends Controller
{

    public function index()
    {
        $hosts = Host::all();
        return view('hosts.index')->with(compact('hosts'));
    }

    public function create()
    {
        return view('hosts.create');
    }

    public function store(Request $request) {

        $request->validate([
            'host_name' => 'required',
        ]);

        $data = $request->all();

        $host = Host::create($data);
        return redirect()->route('hosts.index')->with('success', __('messages.hosts.created'));
    }

    public function edit($id)
    {
        $host = Host::findOrFail($id);
        return view('hosts.edit')->with(compact('host'));
    }

    public function update(Request $request, $id) {

        $request->validate([
            'host_name' => 'required',
        ]);

        $data = $request->all();

        $host = Host::findOrFail($id);
        $host = Host::findOrFail($id);
        $host->update($data);
        $host->save();
        return redirect()->route('hosts.index')->with('success', __('messages.hosts.updated'));
    }

    public function destroy($id) {
        $model = Host::findOrFail($id);

        if($model) {
            $model->deleted_by = Auth::user()->id;
            $model->delete();
            $model->save();
            return redirect()->route('hosts.index')->with('success', __('messages.hosts.deleted'));
        }
        return redirect()->route('hosts.index')->with('error', __('messages.hosts.not_found'));

    }
}
