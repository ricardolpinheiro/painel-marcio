<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Http\Controllers\Controller;
use App\Jobs\SyncTMDBEpisode;
use App\Jobs\SyncTMDBSeason;
use App\TvSerie;
use App\TvSerieSeason;
use App\TvSerieSeasonEpisode;
use App\User;
use Illuminate\Http\Request;

class EpisodeController extends Controller
{
    public $season;
    private $categories;

    public function __construct()
    {
        $categories = Category::where('status', 1)->get();
        $categories = $categories->toArray();

        $this->categories =  parseSelectArray($categories, 'name');
        $this->season =  $this->parseArray();
    }

    public function parseArray() {
        $season = TvSerieSeason::all();
        $res = ['' => 'Selecione'];

        foreach ($season as $season) {
            $res[$season->id] = $season->serie->title . " - Season: " .$season->season_no;
        }
        return $res;
    }

    public function index()
    {
        $episodes = TvSerieSeasonEpisode::all();
        return view('episodes.index')->with(compact('episodes'));
    }

    public function create()
    {
        $seasons = $this->season;
        $categories = $this->categories;
        return view('episodes.create')->with(compact('seasons', 'categories'));
    }

    public function store(Request $request) {

        $request->validate([
            'seasons_id' => 'required|exists:tv_serie_seasons,id',
            "episode_no" => "required",
            "episode_link" => "required",
        ]);

        $data = $request->all();

        $episode = TvSerieSeasonEpisode::create($data);
        dispatch(new SyncTMDBEpisode($episode->season_id, $episode->id));
        return redirect()->route('episodes.index')->with('success', __('messages.episodes.created'));
    }

    public function edit($id)
    {
        $seasons = $this->season;
        $categories = $this->categories;
        $episode = TvSerieSeasonEpisode::findOrFail($id);
        return view('episodes.edit')->with(compact('seasons', 'categories', 'episode'));
    }

    public function update(Request $request, $id) {

        $request->validate([
            'seasons_id' => 'required|exists:tv_serie_seasons,id',
            "episode_no" => "required",
            "episode_link" => "required",
        ]);

        $data = $request->all();

        $category = Category::find($id);
        $category->update($data);
        $category->save();
        return redirect()->route('episodes.index')->with('success', __('messages.episodes.updated'));
    }

    public function destroy($id) {
        $episode = TvSerieSeasonEpisode::find($id);

        if($episode) {
            $episode->delete();
            $episode->save();
            return redirect()->route('episodes.index')->with('success', __('messages.episodes.deleted'));
        }
        return redirect()->route('episodes.index')->with('error', __('messages.episodes.not_found'));

    }
}
