<?php

namespace App\Http\Controllers\Admin;

use App\EpgChannel;
use App\EpgChannelItem;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;

class EpgController extends Controller
{
    public function index()
    {
        $canais = EpgChannel::all();
        return view('epg.index')->with(compact('canais'));
    }

    public function show($id) {
        $canais = EpgChannel::find($id);
        $progs = EpgChannelItem::where('id_canal', '=', $canais->id_canal)->get();
        return view('epg.show')->with(compact('progs'));
    }
}
