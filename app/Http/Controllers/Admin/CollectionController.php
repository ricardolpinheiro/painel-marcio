<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Collection;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CollectionController extends Controller
{
    public function index()
    {
        $collections = Collection::all();
        return view('collections.index')->with(compact('collections'));
    }

    public function show($id) {
        $collection = Collection::find($id);
        return view('collections.show')->with(compact('collection'));
    }

    public function create()
    {
        return view('collections.create');
    }

    public function store(Request $request) {

        $request->validate([
            'name' => 'required',
            'overview' => 'required',
            'backdrop_path' => 'required|mimes:jpeg,bmp,png,jpg',
            'poster_path' => 'required|mimes:jpeg,bmp,png,jpg',
        ]);

        $data = $request->all();

        if($request->hasFile('backdrop_path')) {
            $image = $request->file('backdrop_path');
            $imageName = time() . '.' . $image->getClientOriginalExtension();
            $destinationPath = storage_path('/app/public/collection/');
            $image->move($destinationPath, $imageName);
            $image->imagePath = $destinationPath . $imageName;
            $data['backdrop_path'] = url('storage/collection/' . $imageName);
        }

        if($request->hasFile('poster_path')) {
            $image = $request->file('poster_path');
            $imageName = time() . '.' . $image->getClientOriginalExtension();
            $destinationPath = storage_path('/app/public/collection/');
            $image->move($destinationPath, $imageName);
            $image->imagePath = $destinationPath . $imageName;
            $data['poster_path'] = url('storage/collection/' . $imageName);
        }


        $collection = Collection::create($data);
        return redirect()->route('collections.index')->with('success', __('messages.collections.created'));
    }

    public function edit($id)
    {
        $collection = Collection::find($id);
        return view('collections.edit')->with(compact('collection'));
    }

    public function update(Request $request, $id) {


        $validate = [
            'name' => 'required',
            'overview' => 'required',
            'thumbnail' => 'required|mimes:jpeg,bmp,png,jpg',
            'poster' => 'required|mimes:jpeg,bmp,png,jpg',
        ];

        if(empty($request->get('thumbnail')))
            unset($validate['thumbnail']);

        if(empty($request->get('poster')))
            unset($validate['poster']);

        $request->validate($validate);

        $data = $request->all();

        if($request->hasFile('backdrop_path')) {
            $image = $request->file('backdrop_path');
            $imageName = time() . '.' . $image->getClientOriginalExtension();
            $destinationPath = storage_path('/app/public/collection/');
            $image->move($destinationPath, $imageName);
            $image->imagePath = $destinationPath . $imageName;
            $data['backdrop_path'] = url('storage/collection/' . $imageName);
        } else {
            unset($data['backdrop_path']);
        }

        if($request->hasFile('poster_path')) {
            $image = $request->file('poster_path');
            $imageName = time() . '.' . $image->getClientOriginalExtension();
            $destinationPath = storage_path('/app/public/collection/');
            $image->move($destinationPath, $imageName);
            $image->imagePath = $destinationPath . $imageName;
            $data['poster_path'] = url('storage/collection/' . $imageName);
        } else {
            unset($data['poster_path']);
        }

        $collection = Collection::find($id);
        $collection->update($data);
        $collection->save();
        return redirect()->route('collections.index')->with('success', __('messages.collections.updated'));
    }

    public function destroy($id) {
        $model = Collection::findOrFail($id);

        if($model) {
            $model->deleted_by = Auth::user()->id;
            $model->delete();
            $model->save();
            return redirect()->route('collections.index')->with('success', __('messages.collections.deleted'));
        }
        return redirect()->route('collections.index')->with('error', __('messages.collections.not_found'));

    }
}
