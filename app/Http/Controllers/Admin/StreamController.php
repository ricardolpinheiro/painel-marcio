<?php

namespace App\Http\Controllers\Admin;

use App\Bouquet;
use App\BouquetRelation;
use App\Category;
use App\EpgChannel;
use App\Host;
use App\Http\Controllers\Controller;
use App\Stream;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class StreamController extends Controller
{

    private $categories;
    private $bouquets;
    private $epgs;
    private $hosts;

    public function __construct()
    {
        $categories = Category::where('status', 1)->get();
        $categories = $categories->toArray();

        $bouquets = Bouquet::where('status', 1)->get();
        $bouquets = $bouquets->toArray();

        $epgs = EpgChannel::all();
        $epgs = $epgs->toArray();

        $hosts = Host::all();
        $hosts = $hosts->toArray();

        $this->categories =  parseSelectArray($categories, 'name');
        $this->bouquets =  parseSelectArray($bouquets, 'name');;
        $this->hosts =  parseSelectArray($hosts, 'host_name');;
        $this->epgs =  parseSelectArray($epgs, 'nome', true, 'id_canal');;
    }

    public function index()
    {
        $streams = Stream::all();
        return view('streams.index')->with(compact('streams'));
    }

    public function create()
    {
        $categories = $this->categories;
        $bouquets = $this->bouquets;
        $epgs = $this->epgs;
        $hosts = $this->hosts;

        return view('streams.create')->with(compact('categories', 'bouquets', 'epgs', 'hosts'));
    }

    public function store(Request $request) {
        $request->validate([
            'name' => 'required',
            'stream_logo' => 'required|mimes:jpeg,bmp,png,jpg',
            'stream_link' => 'required|url',
            'category_id' => 'required|exists:categories,id'
        ]);

        $data = $request->all();

        if($request->hasFile('stream_logo')) {
            $image = $request->file('stream_logo');
            $imageName = time() . '.' . $image->getClientOriginalExtension();
            $destinationPath = public_path('/img/stream/');
            $image->move($destinationPath, $imageName);
            $image->imagePath = $destinationPath . $imageName;
            $data['stream_logo'] = url('img/stream/' . $imageName);
        }

        $stream = Stream::create($data);

        if($stream && isset($data['bouquet_id'])) {
            foreach ($data['bouquet_id'] as $bouquet){
                BouquetRelation::create(['resource_id' => $stream->id, 'resource' => 'stream', 'bouquet_id'=>$bouquet]);
            }
        }

        return redirect()->route('streams.index')->with('success', __('messages.streams.created'));
    }

    public function edit($id)
    {
        $categories = $this->categories;
        $bouquets = $this->bouquets;
        $epgs = $this->epgs;
        $hosts = $this->hosts;


        $stream = Stream::findOrFail($id);
        return view('streams.edit')->with(compact('stream', 'categories', 'bouquets', 'epgs', 'hosts'));
    }

    public function update(Request $request, $id) {

        $validate = [
            'name' => 'required',
            'stream_logo' => 'mimes:jpeg,bmp,png,jpg',
            'stream_link' => 'required|url',
            'category_id' => 'required|exists:categories,id'
        ];

        if(empty($request->get('stream_logo')))
            unset($validate['stream_logo']);

        $request->validate($validate);

        $data = $request->all();

        if(empty($request->get('stream_logo')))
            unset($data['stream_logo']);

        if($request->hasFile('stream_logo')) {
            $image = $request->file('stream_logo');
            $imageName = time() . '.' . $image->getClientOriginalExtension();
            $destinationPath = public_path('/img/stream/');
            $image->move($destinationPath, $imageName);
            $image->imagePath = $destinationPath . $imageName;
            $data['stream_logo'] = url('img/stream/' . $imageName);
        }

        $stream = Stream::find($id);
        $stream->update($data);
        $stream->save();

        if($stream){
            $relation = BouquetRelation::where(['resource_id' => $stream->id, 'resource' => 'stream']);
            if($relation->count()) {
                $relation->delete();
            }
            if(isset($data['bouquet_id'])){
                foreach ($data['bouquet_id'] as $bouquet){
                    BouquetRelation::create(['resource_id' => $stream->id, 'resource' => 'stream', 'bouquet_id'=>$bouquet]);
                }
            }

        }


        return redirect()->route('streams.index')->with('success', __('messages.streams.updated'));
    }


    public function destroy($id) {
        $stream = User::find($id);

        if($stream) {
            $stream->deleted_by = Auth::user()->id;
            $stream->delete();
            $stream->save();
            return redirect()->route('streams.index')->with('success', __('messages.streams.deleted'));
        }
        return redirect()->route('streams.index')->with('error', __('messages.streams.not_found'));

    }
}
