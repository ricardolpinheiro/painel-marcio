<?php

namespace App\Http\Controllers\Admin;

use App\Bouquet;
use App\BouquetRelation;
use App\Category;
use App\EpgChannel;
use App\Host;
use App\Http\Controllers\Controller;
use App\Revenda;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RevendaController extends Controller
{

    private $bouquets;

    public function __construct()
    {

        $bouquets = Bouquet::where('status', 1)->get();
        $bouquets = $bouquets->toArray();

        $this->bouquets =  parseSelectArray($bouquets, 'name');;
    }

    public function index()
    {
        $revendas = Revenda::all();
        return view('revendas.index')->with(compact('revendas'));
    }

    public function create()
    {
        $revenda = Revenda::all();
        $bouquets = $this->bouquets;
        return view('revendas.create')->with(compact('revenda', 'bouquets'));
    }

    public function store(Request $request) {

        $request->validate([
            'name' => 'required',
            'username' => 'required|unique:revendas,username',
            'password' => 'required|min:4',
            'path_logo' => 'mimes:jpeg,bmp,png'
        ]);

        $data = $request->all();

        $data['has_painel'] = isset($data['has_painel']) && $data['has_painel'] == 'on' ? true : false;
        $data['expired_at'] = isset($data['is_trial']) && $data['is_trial'] == 'on' ? $data['expired_at'] : '';
        $data['is_trial'] = isset($data['is_trial']) && $data['is_trial'] == 'on' ? true : false;

        if($request->hasFile('path_logo')) {
            $image = $request->file('path_logo');
            $imageName = time() . '.' . $image->getClientOriginalExtension();
            $destinationPath = public_path('/img/revendas/');
            $image->move($destinationPath, $imageName);
            $image->imagePath = $destinationPath . $imageName;
            $data['path_logo'] = url('img/revendas/' . $imageName);
        }

        if($request->hasFile('path_background')) {
            $image = $request->file('path_background');
            $imageName = time() . '.' . $image->getClientOriginalExtension();
            $destinationPath = public_path('/img/revendas/');
            $image->move($destinationPath, $imageName);
            $image->imagePath = $destinationPath . $imageName;
            $data['path_background'] = url('img/revendas/' . $imageName);
        }

        $data['uuid'] = strtoupper(substr($data['name'], 0, 2) . generateRandomString(5));
        $data['created_by'] = Auth::user()->id;

        $revenda = Revenda::create($data);

        if($revenda && isset($data['bouquet_id'])) {
            foreach ($data['bouquet_id'] as $bouquet){
                BouquetRelation::create(['resource_id' => $revenda->id, 'resource' => 'revenda', 'bouquet_id'=>$bouquet]);
            }
        }
        return redirect()->route('revendas.index')->with('success', __('messages.revendas.created'));
    }

    public function edit($id)
    {
        $revenda = Revenda::findOrFail($id);
        $bouquets = $this->bouquets;
        return view('revendas.edit')->with(compact('revenda', 'bouquets'));
    }

    public function update(Request $request, $id) {

        $revenda = Revenda::find($id);
        $data = $request->all();

        if($revenda->username !== $data['username']) {
            $request->validate([
                'name' => 'required',
                'username' => 'required|unique:revendas,username',
                'password' => 'required|min:4',
            ]);
        } else {
            $request->validate([
                'name' => 'required',
                'username' => 'required',
            ]);
        }

        $data['has_painel'] = isset($data['has_painel']) && $data['has_painel'] == 'on' ? true : false;
        $data['expired_at'] = isset($data['is_trial']) && $data['is_trial'] == 'on' ? $data['expired_at'] : '';
        $data['is_trial'] = isset($data['is_trial']) && $data['is_trial'] == 'on' ? true : false;

        $revenda->update($data);
        $revenda->save();

        if($revenda){
            $relation = BouquetRelation::where(['resource_id' => $revenda->id, 'resource' => 'stream']);
            if($relation->count()) {
                $relation->delete();
            }
            if(isset($data['bouquet_id'])){
                foreach ($data['bouquet_id'] as $bouquet){
                    BouquetRelation::create(['resource_id' => $revenda->id, 'resource' => 'revenda', 'bouquet_id'=>$bouquet]);
                }
            }

        }

        return redirect()->route('revendas.index')->with('success', __('messages.revendas.updated'));
    }

    public function password($id) {
        dd($id);
    }
}
