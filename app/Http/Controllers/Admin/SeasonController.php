<?php

namespace App\Http\Controllers\Admin;

use App\Bouquet;
use App\Category;
use App\Http\Controllers\Controller;
use App\Jobs\SyncTMDBSeason;
use App\TvSerie;
use App\TvSerieSeason;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SeasonController extends Controller
{
    public $series;
    public $bouquets;

    public function __construct()
    {
        $series = TvSerie::all();
        $series = $series->toArray();

        $bouquets = Bouquet::where('status', 1)->get();
        $bouquets = $bouquets->toArray();

        $this->bouquets =  parseSelectArray($bouquets, 'name');;
        $this->series =  parseSelectArray($series, 'title');
    }

    public function index()
    {
        $seasons = TvSerieSeason::all();
        return view('seasons.index')->with(compact('seasons'));
    }

    public function create()
    {
        $series = $this->series;

        return view('seasons.create')->with(compact('series'));
    }

    public function store(Request $request) {

        $request->validate([
            'tv_series_id' => 'required|exists:tv_series,id',
            'poster_path' => 'mimes:jpeg,bmp,png,jpg',
        ]);

        $data = $request->all();

        if($request->hasFile('backdrop_path')) {
            $image = $request->file('backdrop_path');
            $imageName = time() . '.' . $image->getClientOriginalExtension();
            $destinationPath = storage_path('/app/public/tvserie/');
            $image->move($destinationPath, $imageName);
            $image->imagePath = $destinationPath . $imageName;
            $data['backdrop_path'] = url('storage/tvserie/' . $imageName);
        }

        if($request->hasFile('poster_path')) {
            $image = $request->file('poster_path');
            $imageName = time() . '.' . $image->getClientOriginalExtension();
            $destinationPath = storage_path('/app/public/tvserie/');
            $image->move($destinationPath, $imageName);
            $image->imagePath = $destinationPath . $imageName;
            $data['poster_path'] = url('storage/tvserie/' . $imageName);
        }


        $season = TvSerieSeason::create($data);
        dispatch(new SyncTMDBSeason($season));
        return redirect()->route('seasons.index')->with('success', __('messages.seasons.created'));
    }

    public function edit($season)
    {
        $season = TvSerieSeason::findOrFail($season);
        $series = $this->series;
        return view('seasons.edit')->with(compact('series', 'season'));
    }

    public function update(Request $request, $id) {

        $season = TvSerieSeason::findOrFail($id);
        $data = $request->all();
        $dispatch = false;

        if($data['season_no'] != $season->season_no || $data['tv_series_id'] != $season->tv_series_id )
            $dispatch = true;


        $validate = [
            "season_no" => "required",
            'tv_series_id' => "required|exists:tv_series,id",
            'poster_path' => 'mimes:jpeg,bmp,png,jpg',
        ];


        if(empty($request->get('poster_path'))){
            unset($validate['poster_path']);
        }

        $request->validate($validate);

        if($request->hasFile('poster_path')) {
            $image = $request->file('poster_path');
            $imageName = time() . '.' . $image->getClientOriginalExtension();
            $destinationPath = storage_path('/app/public/tvserie/');
            $image->move($destinationPath, $imageName);
            $image->imagePath = $destinationPath . $imageName;
            $data['poster_path'] = url('storage/tvserie/' . $imageName);
        } else {
            unset($data['poster_path']);
        }

        $season->update($data);
        $season->save();

        if($dispatch) dispatch(new SyncTMDBSeason($season));

        return redirect()->route('seasons.edit', $season->id)->with('success', __('messages.seasons.updated'));
    }

    public function destroy($id) {
        $season = TvSerieSeason::findOrFail($id);

        if($season) {
            $season->deleted_by = Auth::user()->id;
            $season->delete();
            $season->save();
            return redirect()->route('seasons.index')->with('success', __('messages.seasons.deleted'));
        }
        return redirect()->route('seasons.index')->with('error', __('messages.seasons.not_found'));

    }
}
