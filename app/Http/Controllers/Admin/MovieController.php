<?php

namespace App\Http\Controllers\Admin;

use App\Bouquet;
use App\BouquetRelation;
use App\Category;
use App\Collection;
use App\EpgChannel;
use App\Host;
use App\Http\Controllers\Controller;
use App\Jobs\SyncTMDBMovie;
use App\Movie;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MovieController extends Controller
{
    private $categories;
    private $bouquets;
    private $epgs;
    private $hosts;
    private $collections;

    public function __construct()
    {
        $categories = Category::where('status', 1)->get();
        $categories = $categories->toArray();

        $bouquets = Bouquet::where('status', 1)->get();
        $bouquets = $bouquets->toArray();

        $collections = Collection::all();
        $collections = $collections->toArray();

        $epgs = EpgChannel::all();
        $epgs = $epgs->toArray();

        $hosts = Host::all();
        $hosts = $hosts->toArray();

        $this->categories =  parseSelectArray($categories, 'name');
        $this->bouquets =  parseSelectArray($bouquets, 'name');;
        $this->hosts =  parseSelectArray($hosts, 'host_name');;
        $this->collections =  parseSelectArray($collections, 'name');;
        $this->epgs =  parseSelectArray($epgs, 'nome', true, 'id_canal');;
    }

    public function index()
    {
        $movies = Movie::all();

        return view('movies.index')->with(compact('movies'));
    }

    public function create()
    {
        $categories = $this->categories;
        $bouquets = $this->bouquets;

        return view('movies.create')->with(compact('categories', 'bouquets'));
    }

    public function store(Request $request) {

        $request->validate([
            'title' => 'required_without:tmdb_id',
            'category_id' => 'required|exists:categories,id',
            'movie_link' => 'required|url',
        ]);

        $data = $request->all();
        $movie = Movie::create($data);

        if($movie && isset($data['bouquet_id'])) {
            foreach ($data['bouquet_id'] as $bouquet){
                BouquetRelation::create(['resource_id' => $movie->id, 'resource' => 'movie', 'bouquet_id'=>$bouquet]);
            }
        }

        dispatch(new SyncTMDBMovie($movie));

        return redirect()->route('movies.index')->with('success', __('messages.movies.created'));
    }

    public function edit($id)
    {
        $movie = Movie::find($id);
        $categories = $this->categories;
        $collections = $this->collections;
        $bouquets = $this->bouquets;


        return view('movies.edit')->with(compact('movie', 'categories', 'collections', 'bouquets'));
    }

    public function update(Request $request, $id) {
        $data = $request->all();
        $movie = Movie::findOrFail($id);
        $reload = false;

        if($data['tmdb_id'] !== $movie->tmdb_id || $data['tmdb_id'] == '')
            $reload = true;

        $validate = [
            'category_id' => 'exists:categories,id',
            'movie_link' => 'url',
            'trailer_url' => 'url',
            'thumbnail' => 'mimes:jpeg,bmp,png,jpg',
            'poster' => 'mimes:jpeg,bmp,png,jpg',
        ];

        if(empty($request->get('thumbnail')))
            unset($validate['thumbnail']);

        if(empty($request->get('poster')))
            unset($validate['poster']);

        if(empty($request->get('trailer_url'))) {
            unset($data['trailer_url']);
            unset($validate['trailer_url']);
        }

        $request->validate($validate);

        if($request->hasFile('thumbnail')) {
            $image = $request->file('thumbnail');
            $imageName = time() . '.' . $image->getClientOriginalExtension();
            $destinationPath = storage_path('/app/public/movie/');
            $image->move($destinationPath, $imageName);
            $image->imagePath = $destinationPath . $imageName;
            $data['thumbnail'] = url('storage/movie/' . $imageName);
        } else {
            unset($data['thumbnail']);
        }

        if($request->hasFile('poster')) {
            $image = $request->file('poster');
            $imageName = time() . '.' . $image->getClientOriginalExtension();
            $destinationPath = storage_path('/app/public/movie/');
            $image->move($destinationPath, $imageName);
            $image->imagePath = $destinationPath . $imageName;
            $data['poster'] = url('storage/movie/' . $imageName);
        }else {
            unset($data['poster']);
        }

        $movie->update($data);
        $movie->save();

        if($movie){
            $relation = BouquetRelation::where(['resource_id' => $movie->id, 'resource' => 'movie']);
            if($relation->count()) {
                $relation->delete();
            }
            if(isset($data['bouquet_id'])){
                foreach ($data['bouquet_id'] as $bouquet){
                    BouquetRelation::create(['resource_id' => $movie->id, 'resource' => 'movie', 'bouquet_id'=>$bouquet]);
                }
            }

        }

        if($reload) dispatch(new SyncTMDBMovie($movie));
        return redirect()->route('movies.index')->with('success', __('messages.movies.updated'));
    }

    public function destroy($id) {
        $movie = Movie::find($id);

        if($movie) {
            $movie->deleted_by = Auth::user()->id;
            $movie->delete();
            $movie->save();
            return redirect()->route('movies.index')->with('success', __('messages.movies.deleted'));
        }
        return redirect()->route('movies.index')->with('error', __('messages.movies.not_found'));

    }
}
