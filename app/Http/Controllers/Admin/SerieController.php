<?php

namespace App\Http\Controllers\Admin;

use App\Bouquet;
use App\BouquetRelation;
use App\Category;
use App\Http\Controllers\Controller;
use App\Jobs\SyncTMDBMovie;
use App\Jobs\SyncTMDBSerie;
use App\Movie;
use App\TvSerie;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SerieController extends Controller
{
    public $bouquets;

    public function __construct()
    {
        $bouquets = Bouquet::where('status', 1)->get();
        $bouquets = $bouquets->toArray();
        $this->bouquets =  parseSelectArray($bouquets, 'name');;
    }

    public function index()
    {
        $series = TvSerie::all();
        return view('series.index')->with(compact('series'));
    }

    public function create()
    {
        $bouquets = $this->bouquets;
        return view('series.create')->with(compact('bouquets'));
    }

    public function store(Request $request) {

        $request->validate([
            'tmdb_id' => 'required_without:title',
            'title' => 'required_without:tmdb_id',
            'poster_path' => 'mimes:jpeg,bmp,png,jpg',
            'backdrop_path' => 'mimes:jpeg,bmp,png,jpg',
        ]);

        $data = $request->all();

        if($request->hasFile('backdrop_path')) {
            $image = $request->file('backdrop_path');
            $imageName = time() . '.' . $image->getClientOriginalExtension();
            $destinationPath = storage_path('/app/public/tvserie/');
            $image->move($destinationPath, $imageName);
            $image->imagePath = $destinationPath . $imageName;
            $data['backdrop_path'] = url('storage/tvserie/' . $imageName);
        }

        if($request->hasFile('poster_path')) {
            $image = $request->file('poster_path');
            $imageName = time() . '.' . $image->getClientOriginalExtension();
            $destinationPath = storage_path('/app/public/tvserie/');
            $image->move($destinationPath, $imageName);
            $image->imagePath = $destinationPath . $imageName;
            $data['poster_path'] = url('storage/tvserie/' . $imageName);
        }

        $serie = TvSerie::create($data);

        dispatch(new SyncTMDBSerie($serie));

        if($serie && isset($data['bouquet_id'])) {
            foreach ($data['bouquet_id'] as $bouquet){
                BouquetRelation::create(['resource_id' => $serie->id, 'resource' => 'serie', 'bouquet_id'=>$bouquet]);
            }
        }

        return redirect()->route('series.index')->with('success', __('messages.series.created'));
    }

    public function edit($id)
    {
        $serie = TvSerie::findOrFail($id);
        $bouquets = $this->bouquets;

        return view('series.edit')->with(compact('serie', 'bouquets'));
    }

    public function update(Request $request, $id) {
        $data = $request->all();
        $serie = TvSerie::find($id);
        $dispatch = false;

        if($data['tmdb_id'] != $serie->tmdb_id || !empty($serie->tmdb_id) || !is_null($serie->tmdb_id)) {
            $dispatch = true;
        }


        $validate = [
            'title' => 'required',
            'detail' => 'required',
            'poster_path' => 'required|mimes:jpeg,bmp,png,jpg',
            'backdrop_path' => 'required|mimes:jpeg,bmp,png,jpg',
        ];
        $request->validate($validate);

        if(empty($request->get('poster_path'))){
            unset($validate['poster_path']);
        }

        if(empty($request->get('backdrop_path'))){
            unset($validate['backdrop_path']);
        }

        if($request->hasFile('backdrop_path')) {
            $image = $request->file('backdrop_path');
            $imageName = time() . '.' . $image->getClientOriginalExtension();
            $destinationPath = storage_path('/app/public/tvserie/');
            $image->move($destinationPath, $imageName);
            $image->imagePath = $destinationPath . $imageName;
            $data['backdrop_path'] = url('storage/tvserie/' . $imageName);
        } else {
            unset($data['backdrop_path']);
        }

        if($request->hasFile('poster_path')) {
            $image = $request->file('poster_path');
            $imageName = time() . '.' . $image->getClientOriginalExtension();
            $destinationPath = storage_path('/app/public/tvserie/');
            $image->move($destinationPath, $imageName);
            $image->imagePath = $destinationPath . $imageName;
            $data['poster_path'] = url('storage/tvserie/' . $imageName);
        } else {
            unset($data['poster_path']);
        }

        $serie->update($data);
        $serie->save();

         if($serie){
            $relation = BouquetRelation::where(['resource_id' => $serie->id, 'resource' => 'stream']);
            if($relation->count()) {
                $relation->delete();
            }

            if(isset($data['bouquet_id'] )) {
                foreach ($data['bouquet_id'] as $bouquet){
                    BouquetRelation::create(['resource_id' => $serie->id, 'resource' => 'serie', 'bouquet_id'=>$bouquet]);
                }
            }

        }
        if($dispatch) dispatch(new SyncTMDBSerie($serie));

        return redirect()->route('series.index')->with('success', __('messages.series.updated'));
    }

    public function destroy($id) {
        $serie = TvSerie::find($id);

        if($serie) {
            $serie->delete();
            $serie->save();
            return redirect()->route('series.index')->with('success', __('messages.series.deleted'));
        }
        return redirect()->route('series.index')->with('error', __('messages.series.not_found'));

    }
}
