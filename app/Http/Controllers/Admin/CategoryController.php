<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CategoryController extends Controller
{

    public function index()
    {
        $categories = Category::all();
        return view('categories.index')->with(compact('categories'));
    }

    public function create()
    {
        return view('categories.create');
    }

    public function store(Request $request) {

        $request->validate([
            'name' => 'required',
        ]);

        $data = $request->all();

        $category = Category::create($data);
        return redirect()->route('categories.index')->with('success', __('messages.categories.created'));
    }

    public function edit($id)
    {
        $category = Category::find($id);
        return view('categories.edit')->with(compact('category'));
    }

    public function update(Request $request, $id) {

        $request->validate([
            'name' => 'required',
        ]);

        $data = $request->all();

        $category = Category::find($id);
        $category->update($data);
        $category->save();
        return redirect()->route('categories.index')->with('success', __('messages.categories.updated'));
    }

    public function destroy($id) {
        $model = Category::findOrFail($id);

        if($model) {
            $model->deleted_by = Auth::user()->id;
            $model->delete();
            $model->save();
            return redirect()->route('categories.index')->with('success', __('messages.categories.deleted'));
        }
        return redirect()->route('categories.index')->with('error', __('messages.categories.not_found'));

    }
}
