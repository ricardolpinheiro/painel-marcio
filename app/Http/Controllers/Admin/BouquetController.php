<?php

namespace App\Http\Controllers\Admin;

use App\Bouquet;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BouquetController extends Controller
{

    public function index()
    {
        $bouquets = Bouquet::all();
        return view('bouquets.index')->with(compact('bouquets'));
    }

    public function create()
    {
        return view('bouquets.create');
    }

    public function store(Request $request) {

        $request->validate([
            'name' => 'required',
        ]);

        $data = $request->all();

        $bouquet = Bouquet::create($data);
        return redirect()->route('bouquets.index')->with('success', __('messages.bouquets.created'));
    }

    public function edit($id)
    {
        $bouquet = Bouquet::findOrFail($id);
        return view('bouquets.edit')->with(compact('bouquet'));
    }

    public function update(Request $request, $id) {

        $request->validate([
            'name' => 'required',
        ]);

        $data = $request->all();

        $bouquet = Bouquet::findOrFail($id);
        $bouquet = Bouquet::findOrFail($id);
        $bouquet->update($data);
        $bouquet->save();
        return redirect()->route('bouquets.index')->with('success', __('messages.bouquets.updated'));
    }


    public function destroy($id) {
        $bouquet = Bouquet::findOrFail($id);

        if($bouquet) {
            $bouquet->deleted_by = Auth::user()->id;
            $bouquet->delete();
            $bouquet->save();
            return redirect()->route('bouquets.index')->with('success', __('messages.bouquets.deleted'));
        }
        return redirect()->route('bouquets.index')->with('error', __('messages.bouquets.not_found'));

    }
}
