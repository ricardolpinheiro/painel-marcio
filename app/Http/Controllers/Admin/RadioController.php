<?php

namespace App\Http\Controllers\Admin;

use App\Bouquet;
use App\BouquetRelation;
use App\Category;
use App\Http\Controllers\Controller;
use App\Radio;
use App\Stream;
use App\TvSerieSeason;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RadioController extends Controller
{

    private $categories;
    private $bouquets;

    public function __construct()
    {
        $categories = Category::where('status', 1)->get();
        $categories = $categories->toArray();

        $bouquets = Bouquet::where('status', 1)->get();
        $bouquets = $bouquets->toArray();

        $this->categories =  parseSelectArray($categories, 'name');
        $this->bouquets =  parseSelectArray($bouquets, 'name');;
    }

    public function index()
    {
        $radios = Radio::all();
        return view('radios.index')->with(compact('radios'));
    }

    public function create()
    {
        $categories = $this->categories;
        $bouquets = $this->bouquets;

        return view('radios.create')->with(compact('categories', 'bouquets'));
    }

    public function store(Request $request) {

        $request->validate([
            'name' => 'required',
            'stream_logo' => 'required|mimes:jpeg,bmp,png,jpg',
            'stream_link' => 'required|url',
            'category_id' => 'required|exists:categories,id'
        ]);

        $data = $request->all();

        if($request->hasFile('stream_logo')) {
            $image = $request->file('stream_logo');
            $imageName = time() . '.' . $image->getClientOriginalExtension();
            $destinationPath = public_path('/img/radio/');
            $image->move($destinationPath, $imageName);
            $image->imagePath = $destinationPath . $imageName;
            $data['stream_logo'] = url('img/radio/' . $imageName);
        }

        $radio = Radio::create($data);
        if($radio && isset($data['bouquet_id'])) {
            foreach ($data['bouquet_id'] as $bouquet){
                BouquetRelation::create(['resource_id' => $radio->id, 'resource' => 'stream', 'bouquet_id'=>$bouquet]);
            }
        }

        return redirect()->route('radios.index')->with('success', __('messages.radios.created'));
    }

    public function edit($id)
    {
        $categories = $this->categories;
        $bouquets = $this->bouquets;

        $radio = Radio::findOrFail($id);
        return view('radios.edit')->with(compact('radio', 'categories', 'bouquets'));
    }

    public function update(Request $request, $id) {

        $validate = [
            'name' => 'required',
            'stream_logo' => 'mimes:jpeg,bmp,png,jpg',
            'stream_link' => 'required|url',
            'category_id' => 'required|exists:categories,id'
        ];

        if(empty($request->get('stream_logo')))
            unset($validate['stream_logo']);

        $request->validate($validate);

        $data = $request->all();

        if(empty($request->get('stream_logo')))
            unset($data['stream_logo']);

        if($request->hasFile('stream_logo')) {
            $image = $request->file('stream_logo');
            $imageName = time() . '.' . $image->getClientOriginalExtension();
            $destinationPath = public_path('/img/radio/');
            $image->move($destinationPath, $imageName);
            $image->imagePath = $destinationPath . $imageName;
            $data['stream_logo'] = url('img/radio/' . $imageName);
        }

        $radio = Radio::find($id);
        $radio->update($data);
        $radio->save();

        if($radio){
            $relation = BouquetRelation::where(['resource_id' => $radio->id, 'resource' => 'stream']);
            if($relation->count()) {
                $relation->delete();
            }

            if(isset($data['bouquet_id'])) {
                foreach ($data['bouquet_id'] as $bouquet){
                    BouquetRelation::create(['resource_id' => $radio->id, 'resource' => 'stream', 'bouquet_id'=>$bouquet]);
                }
            }

        }


        return redirect()->route('radios.index')->with('success', __('messages.radios.updated'));
    }

    public function destroy($id) {
        $radio = Radio::findOrFail($id);

        if($radio) {
            $radio->deleted_by = Auth::user()->id;
            $radio->delete();
            $radio->save();
            return redirect()->route('radios.index')->with('success', __('messages.radios.deleted'));
        }
        return redirect()->route('radios.index')->with('error', __('messages.radios.not_found'));

    }
}
