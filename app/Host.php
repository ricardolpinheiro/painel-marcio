<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

class Host extends Model implements Auditable
{
    use  \OwenIt\Auditing\Auditable, SoftDeletes;

    protected $fillable = ['host_name'];

    public function stream() {
        return $this->belongsToMany(Host::class, 'streams', 'host_id');
    }
}
