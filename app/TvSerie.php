<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

class TvSerie extends Model implements Auditable
{
    use  \OwenIt\Auditing\Auditable, SoftDeletes;

    protected $fillable = [
        "title",
        "original_name",
        "tmdb_id",
        "category_id",
        "poster_path",
        "backdrop_path",
        "detail",
        "rating",
        "type",
    ];

    public function seasons() {
        return $this->hasMany(TvSerieSeason::class, 'tv_series_id', 'id');
    }

    public function bouquets() {
        return $this->hasMany(BouquetRelation::class, 'resource_id', 'id');
    }

    public function available_bouquets() {
        return $this->bouquets()->where('resource', '=', 'serie');
    }

}
