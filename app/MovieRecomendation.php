<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

class MovieRecomendation extends Model implements Auditable
{
    use  \OwenIt\Auditing\Auditable, SoftDeletes;


    protected $fillable = [
        "title",
        "backdrop_path",
        "tmdb_id",
        "movie_id",
    ];
}
