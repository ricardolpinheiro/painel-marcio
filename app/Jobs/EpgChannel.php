<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;
use Ixudra\Curl\Facades\Curl;

class EpgChannel implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $url;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        $rows = env('EPG_ROW', 10);
        $this->url = "https://programacao.netcombo.com.br/gatekeeper/canal/select?q=id_cidade:1&wt=json&rows=$rows&start=0&sort=cn_canal%20asc&fl=id_canal%20st_canal%20cn_canal%20nome%20url_imagem&fq=nome:*&_=1567799768090";
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        $response = Curl::to($this->url)
            ->withHeader('User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36')
            ->withHeader('Sec-Fetch-Mode: no-cors')
            ->withHeader('Referer: https://www.net.com.br/tv-por-assinatura/programacao/grade')
            ->get();

        $data = json_decode($response, true);
        $canais = $data['response']['docs'];

        foreach ($canais as $canal) {
            $find = \App\EpgChannel::where(['id_canal' => $canal['id_canal']])->count();
            if($find == 0) {
                \App\EpgChannel::create($canal);
            }
        }
    }
}
