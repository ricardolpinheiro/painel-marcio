<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Ixudra\Curl\Facades\Curl;

class EpgChannelItem implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $url;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->url = "https://programacao.netcombo.com.br/gatekeeper/exibicao/select?q={#ID}json.wrf=callbackShows&wt=json&rows=100000&sort=id_canal%20asc,dh_inicio%20asc&fl=dh_fim%20dh_inicio%20st_titulo%20titulo%20id_programa%20id_canal&fq=dh_inicio:%5B2020-03-16T00:00:00Z%20TO%202020-03-16T23:59:00Z%5D";
        $this->url = "https://programacao.netcombo.com.br/gatekeeper/exibicao/select?q={#ID}&wt=json&rows=100000&sort=id_canal%20asc,dh_inicio%20asc&fl=dh_fim%20dh_inicio%20st_titulo%20titulo%20id_programa%20id_canal&fq=dh_inicio:[2020-04-06T00:00:00Z%20TO%202020-04-06T23:59:00Z]";
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        set_time_limit (1000);

        $canais = \App\EpgChannel::all();
        $str = 'id_revel:1_';
        $total = $canais->count();

        $arr = [];

        $skip = 0;
        for ($i=1; $i<=28;$i++) {

            $arr[$i] =\App\EpgChannel::skip($skip)->take(10)->get();
            $skip +=10;
        }

        $str_final = '';
        foreach ($arr as $item) {

            foreach ($item as $epg) {
                $str_final = $str_final . $str . $epg->id_canal . '+';
            }

            $url = $this->url;
            $url = str_replace('{#ID}', $str_final, $url);

            $response = Curl::to($url)
                ->withHeader('User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36')
                ->withHeader('Sec-Fetch-Mode: no-cors')
                ->withHeader('Referer: https://www.net.com.br/tv-por-assinatura/programacao/grade')
                ->get();

            $data = json_decode($response, true);
            $programacao = $data['response']['docs'];

            foreach ($programacao as $item){
                $find = \App\EpgChannelItem::where('dh_inicio', '=', $item['dh_inicio'])->where('id_programa', '=', $item['id_programa']);
                if($find->count() == 0) {
                    $item['dh_inicio'] = str_replace('Z', '', str_replace('T', ' ',  $item['dh_inicio']));
                    $item['dh_fim'] = str_replace('Z', '', str_replace('T', ' ',  $item['dh_fim']));
                    \App\EpgChannelItem::create($item);
                }
            }

            $str_final = '';
            sleep(1);
        }

        return ;
//
//        $response = Curl::to($url)
//            ->withHeader('User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36')
//            ->withHeader('Sec-Fetch-Mode: no-cors')
//            ->withHeader('Referer: https://www.net.com.br/tv-por-assinatura/programacao/grade')
//            ->get();
//
//
//

    }
}
