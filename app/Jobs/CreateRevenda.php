<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class CreateRevenda implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    /**
     * @var Revenda
     */
    private Revenda $revenda;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Revenda $revenda)
    {
        //
        $this->revenda = $revenda;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        exec("git clone https://ricardolpinheiro@bitbucket.org/ricardolpinheiro/revenda-f.git " . public_path('revenda'));
    }
}
