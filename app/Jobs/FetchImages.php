<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\URL;
use Ixudra\Curl\Facades\Curl;
use \Gumlet\ImageResize;

class FetchImages implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    private $res;
    private $model;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($res, $model)
    {
        $this->res = $res;
        $this->model = $model;

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        switch ($this->res) {
            case 'movie':
                $this->getResourceMovie();
                break;
            case 'serie':
                $this->getResourceSerie();
                break;
            case 'season':
                $this->getResourceSeason();
                break;
            case 'episode':
                $this->getResourceEpisode();
                break;
            case 'collection';
                $this->getResourceCollection();
                break;
            default:
                return;
        }
    }

    function checkExists($url) {
        $response = Curl::to($url)
            ->returnResponseObject()
            ->withTimeout(0)
            ->asJson()
            ->get();

        if($response->status == 200) {
            return true;
        } else {
            Log::info("Status != 200: " . $url);
            return false;
        }
    }

    function getResourceEpisode() {
        if(!empty($this->model->thumbnail)) {
            $url = "https://image.tmdb.org/t/p/original" . $this->model->thumbnail;
            if($this->checkExists($url)) {
                $contents = file_get_contents($url);
                $name = '/public/original/'.  substr($url, strrpos($url, '/') + 1);
                Storage::put($name, $contents);
                $image = new ImageResize(storage_path("app" . $name));
                $image->resizeToWidth(300);
                $image->save(storage_path("app" . $name));
                $this->model->thumbnail = URL::to(str_replace('/public', '/storage', $name));
                $this->model->save();
            }

        }
    }

    public function getResourceSerie(){


        if(!empty($this->model->poster_path)) {
            $url = "https://image.tmdb.org/t/p/original" . $this->model->poster_path;
            if($this->checkExists($url)) {
                $contents = file_get_contents($url);

                $name = '/public/original/'. substr($url, strrpos($url, '/') + 1);
                Storage::put($name, $contents);
                $image = new ImageResize(storage_path("app" . $name));
                $image->resizeToWidth(300);
                $image->save(storage_path("app" . $name));
                $this->model->poster_path = URL::to(str_replace('/public', '/storage', $name));
                $this->model->save();
            }
        }

        if(!empty($this->model->backdrop_path)) {
            $url = "https://image.tmdb.org/t/p/original" . $this->model->backdrop_path;
            if($this->checkExists($url)) {
                $contents = file_get_contents($url);

                $name = '/public/original/'. substr($url, strrpos($url, '/') + 1);
                Storage::put($name, $contents);
                $image = new ImageResize(storage_path("app" . $name));
                $image->resizeToWidth(800);
                $image->save(storage_path("app" . $name));
                $this->model->backdrop_path = URL::to(str_replace('/public', '/storage', $name));
                $this->model->save();
            }
        }
    }

    public function getResourceSeason(){

        if(!empty($this->model->poster_path)) {
            $url = "https://image.tmdb.org/t/p/original" . $this->model->poster_path;
            if($this->checkExists($url)) {
                $contents = file_get_contents($url);
                $name = '/public/original/' . substr($url, strrpos($url, '/') + 1);
                Storage::put($name, $contents);
                $image = new ImageResize(storage_path("app" . $name));
                $image->resizeToWidth(300);
                $image->save(storage_path("app" . $name));
                $this->model->poster_path = URL::to(str_replace('/public', '/storage', $name));
                $this->model->save();
            }
        }
    }

    public function getResourceMovie(){
        if(!empty($this->model->thumbnail && $this->model->thumbnail != '')) {
            $url = "https://image.tmdb.org/t/p/original" . $this->model->thumbnail;
            if($this->checkExists($url)) {

                $contents = @file_get_contents($url);
                $name = '/public/original/'. substr($url, strrpos($url, '/') + 1);
                Storage::put($name, $contents);
                $image = new ImageResize(storage_path("app" . $name));
                $image->resizeToWidth(800);
                $image->save(storage_path("app" . $name));
                $this->model->thumbnail = URL::to(str_replace('/public', '/storage', $name));
                $this->model->save();
            }

        }

        if(!empty($this->model->poster && $this->model->poster != '')) {
            $url = "https://image.tmdb.org/t/p/w500" . $this->model->poster;
            if($this->checkExists($url)) {
                $contents = @file_get_contents($url);
                $name = '/public/original/'. substr($url, strrpos($url, '/') + 1);
                Storage::put($name, $contents);
                $image = new ImageResize(storage_path("app" . $name));
                $image->resizeToWidth(300);
                $image->save(storage_path("app" . $name));
                $this->model->poster = URL::to(str_replace('/public', '/storage', $name));
                $this->model->save();
            }

        }
    }

    public function getResourceCollection(){
    ;

        if(!empty($this->model->poster_path)) {
            $url = "https://image.tmdb.org/t/p/original" . $this->model->poster_path;
            if($this->checkExists($url)) {
                $contents = @file_get_contents($url);

                $name = '/public/original/'. substr($url, strrpos($url, '/') + 1);
                Storage::put($name, $contents);

                $image = new ImageResize(storage_path("app" . $name));
                $image->resizeToWidth(800);
                $image->save(storage_path("app" . $name));

                $this->model->poster_path = URL::to(str_replace('/public', '/storage', $name));
                $this->model->save();
            }

        }

        if(!empty($this->model->backdrop_path) && $this->model->backdrop_path != '') {
            $url = "https://image.tmdb.org/t/p/w500" . $this->model->backdrop_path;
            if($this->checkExists($url)) {
                $contents = @file_get_contents($url);
                $name = '/public/original/'. substr($url, strrpos($url, '/') + 1);
                Storage::put($name, $contents);

                $image = new ImageResize(storage_path("app" . $name));
                $image->resizeToWidth(300);
                $image->save(storage_path("app" . $name));

                $this->model->backdrop_path = URL::to(str_replace('/public', '/storage', $name));
                $this->model->save();
            }

        }

    }

}
