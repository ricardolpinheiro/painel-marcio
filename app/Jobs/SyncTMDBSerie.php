<?php

namespace App\Jobs;

use App\Genre;
use App\GenreMovie;
use App\GenreSerie;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class SyncTMDBSerie implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $serie;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($serie)
    {
        $this->serie = $serie;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if(empty($this->serie->tmdb_id)) {
            $this->syncByName();
        } else {
            $this->syncById();
        }
    }

    function syncByName() {
        $serie = tmdbGetSerieByName($this->serie->title);
        if($serie->total_results > 0) {
            $this->serie->title = $serie->results[0]->name;
            $this->serie->tmdb_id = $serie->results[0]->id;
            $this->serie->original_name = $serie->results[0]->original_name;
            $this->serie->save();
            $this->syncById();
        }
    }

    function syncById() {
        $serie = tmdbGetSerieById($this->serie->tmdb_id);

        if(isset($serie) && isset($serie->id)) {
            $this->serie->title = $serie->name;
            $this->serie->original_name = $serie->original_name;
            $this->serie->poster_path = $serie->poster_path;
            $this->serie->backdrop_path = $serie->backdrop_path;
            $this->serie->detail = $serie->overview;
            $this->serie->rating = $serie->vote_average;
            $this->serie->save();
            $this->assertGenres($serie);
            dispatch(new FetchImages('serie',  $this->serie));
        }
    }

    public function assertGenres($serie) {
        if(isset($serie->genres)) {
            foreach ($serie->genres as $serie) {
                $find = Genre::where('tmdb_id', '=', $serie->id)->get();
                if(!$find->count()) {
                    $g['name'] = $serie->name;
                    $g['tmdb_id'] = $serie->id;
                    $find = Genre::create($g);
                    GenreSerie::create(['serie_id' => $this->serie->id, 'genre_id' => $find->id]);
                } else {
                    $find =  $find[0];
                    $gs = GenreSerie::where('serie_id', '=',  $this->serie->id)->where('genre_id', '=', $find->id)->get();
                    if(!$gs->count() > 0) {
                        GenreSerie::create(['serie_id' => $this->serie->id, 'genre_id' => $find->id]);
                    }
            }
        }
    }
}
}
