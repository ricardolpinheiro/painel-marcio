<?php

namespace App\Jobs;

use App\Collection;
use App\Genre;
use App\GenreMovie;
use App\GenreSerie;
use App\Movie;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class SyncTMDBMovie implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var Movie
     */
    private $movie;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Movie $movie)
    {
        $this->movie = $movie;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(){
        set_time_limit(300);
        if(empty($this->movie->tmdb_id)) {
            $this->syncByName();
        } else {
            $this->syncById();
        }
    }

    public function syncByName() {
        $movie = tmdbGetMovieByName($this->movie->title);

        if($movie->total_results > 0){

            $this->movie->tmdb_id = $movie->results[0]->id;
            $lançamento = explode('-', $movie->results[0]->release_date);
            $this->movie->released = $lançamento[2] . '/' . $lançamento[1]. '/' . $lançamento[0];
            $this->movie->save();
            $this->syncById();
        }
    }

    public function syncById() {
        $movie = tmdbGetMovieById($this->movie->tmdb_id);

        if(isset($movie)) {
            $this->movie->title = $movie->title;
            $release = explode('-', $movie->release_date);
            $this->movie->released = $release[2] . '/' . $release[1] . '/' . $release[0];
            $this->movie->adult = $movie->adult;
            $this->movie->duration = $movie->runtime;
            $this->movie->thumbnail = $movie->backdrop_path;
            $this->movie->poster = $movie->poster_path;
            $this->movie->detail = $movie->overview;
            $this->movie->rating = $movie->vote_average;
            $this->movie->collection_id = $this->assertCollection($movie);

            $this->assertGenres($movie);
            $this->movie->status = 'pending';
            $this->movie->save();

            dispatch(new FetchImages('movie', $this->movie));
            dispatch(new FetchRecomendation('M', $this->movie));
        }
    }

    public function assertCollection($movie) {

        if($movie->belongs_to_collection) {

            $collection = Collection::where('tmdb_id', '=', $movie->belongs_to_collection->id)->get();

            if(!empty($movie->belongs_to_collection) && $collection->count() === 0) {
                $collection = $this->getDetailCollection($movie->belongs_to_collection);
                if($collection->count()){
                    return $collection->id;
                }
            } else {

                if($collection->count() > 0) {
                    return $collection[0]->id;
                }
            }
        }

        return null;
    }

    public function assertGenres($movie) {
        if(isset($movie->genres)) {
            foreach ($movie->genres as $genre) {
                $find = Genre::where('tmdb_id', '=', $genre->id)->get();
                if(!$find->count()) {
                    $g['name'] = $genre->name;
                    $g['tmdb_id'] = $genre->id;
                    $find = Genre::create($g);
                    GenreMovie::create(['movie_id' => $this->movie->id, 'genre_id' => $find->id]);
                } else {
                    $find =  $find[0];
                    $gs = GenreMovie::where(['movie_id' => $this->movie->id, 'genre_id' => $find->id])->get();

                    if(!$gs->count() > 0) {
                        GenreMovie::create(['movie_id' => $this->movie->id, 'genre_id' => $find->id]);

                    }
                }

            }
        }
    }

    public function getDetailCollection($data) {

        $data = tmdbGetCollectionById($data->id);

        if(isset($data) && isset($data->id)) {
            $collection['tmdb_id'] = $data->id;
            $collection['name'] = $data->name;
            $collection['overview'] = $data->overview;
            $collection['poster_path'] = $data->poster_path;
            $collection['backdrop_path'] = $data->backdrop_path;
            $model = Collection::create($collection);

            dispatch(new FetchImages('collection', $model));
            return $model;
        }
        return null;
    }

}
