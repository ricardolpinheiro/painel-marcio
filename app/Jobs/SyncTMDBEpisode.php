<?php

namespace App\Jobs;

use App\TvSerieSeasonEpisode;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class SyncTMDBEpisode implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    private $season;
    private $episode;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($season, $episode)
    {
        //
        $this->season = $season;
        $this->episode = $episode;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if(empty($this->season) && empty($this->episode)) {
            return;
        }
        $episode = TvSerieSeasonEpisode::find($this->episode);

        $season = tmdbGetEpisodeBySerie($episode->season->serie->tmdb_id, $episode->season->season_no, $episode->episode_no);
        if(isset($season)) {
            $episode->title = $season->name;
            $episode->detail = $season->overview;
            $episode->thumbnail = $season->still_path;
            $episode->save();
            dispatch(new FetchImages('episode', $episode));
        }
    }
}
