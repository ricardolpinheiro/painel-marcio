<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class SyncTMDBSeason implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    private $season;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($season)
    {
        $this->season = $season;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if(empty($this->season->season_no) && empty($this->season->tv_series_id)) {
            return;
        }

        $season = tmdbGetSeasonBySerie($this->season->serie->tmdb_id, $this->season->season_no);

        if(isset($season)) {
            $this->season->poster_path = $season->poster_path;
            $this->season->tmdb_id = $season->id;
            $this->season->detail = $season->overview;
            $this->season->save();
            dispatch(new FetchImages('season', $this->season));
        }
    }
}
