<?php

namespace App\Jobs;

use App\Movie;
use App\MovieRecomendation;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class FetchRecomendation implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    private $type;
    private $model;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($type, $model)
    {
        //
        $this->type = $type;
        $this->model = $model;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        switch ($this->type) {
            case 'M':
                $this->getResourceMovie();
            default:
                return;
        }
    }

    public function getResourceMovie() {
        //$term = 'https://api.themoviedb.org/3/movie/'.$this->model->tmdb_id.'/recommendations?api_key=' . TMDB_API_KEY . '&language=pt-BR&page=1';
//        $search_data = @file_get_contents($term);
//
//        if ( $search_data ) {
//            $data = json_decode( $search_data, true );
//            if(isset($data['results'])) {
//                foreach ($data['results'] as $res) {
//                    $movie = Movie::where('tmdb_id', '=', $res['id'])->get();
//                    if($movie->count() && !empty($movie->poster)) {
//                        $movie = $movie[0];
//                        $rec['title'] = $res['title'];
//                        $rec['backdrop_path'] = $movie->poster;
//                        $rec['movie_id'] = $this->model->id;
//                        $rec['tmdb_id'] = $res['id'];
//                        $recomendation = MovieRecomendation::create($rec);
//                    }
//                }
//            }
//        }
    }
}
