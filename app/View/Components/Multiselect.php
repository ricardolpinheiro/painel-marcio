<?php

namespace App\View\Components;

use Illuminate\View\Component;

class Multiselect extends Component
{
    public $name;
    public $text;
    public $placeholder;
    public $icon;
    public $required;
    public $error;

    public $message;
    public $hasError;
    public $value;

    public $type;
    public $id;
    /**
     * @var array
     */
    public $options;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($name, $text='', $placeholder='', $options=[], $type="text", $icon='', $error = null, $required=false, $value='')
    {

        $this->name = $name;
        $this->text = empty($text) ? ucfirst($name) : $text;
        $this->placeholder = empty($placeholder) ? ucfirst($name) : $placeholder;
        $this->icon = $icon;
        $this->required = $required;

        $this->message = $this->getMessage($name, $error->getMessageBag());
        $this->hasError = !empty($this->message) ? true : false;

        $this->value = $value !== '' ?  parseSelectArray($value->toArray(), 'bouquet_id') : [];
        $this->type = $type;
        $this->options = $options;
        $this->id = str_replace(']', '', str_replace('[', '', $name));
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.multiselect');
    }

    public function getMessage($name, $errors=[]) {
        $keys = array_keys($errors->messages());
        $pos = in_array($name, $keys);

        if($pos) {
            return $errors->get($name)[0];
        }
    }
}
