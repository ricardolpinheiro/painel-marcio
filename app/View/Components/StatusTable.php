<?php

namespace App\View\Components;

use Illuminate\View\Component;

class StatusTable extends Component
{
    public $status;
    public $tooltip;
    /**
     * @var string
     */
    public $color;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($status, $color='',$tooltip='')
    {
        $this->status = $status;
        $this->tooltip = $tooltip;

        $this->status = $status == 1 ? 'green' : 'red';
        $this->tooltip = $status == 1 ? 'Ativo' : 'Inativo';

        $this->color = $status == 1 ? 'green' : 'red';
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.status-table');
    }
}
