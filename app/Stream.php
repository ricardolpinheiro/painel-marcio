<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

class Stream extends Model implements Auditable
{
    use  \OwenIt\Auditing\Auditable, SoftDeletes;

    protected $fillable = [
        'name',
        'category_id',
        'epg_id',
        'host_id',
        'stream_logo',
        'stream_link',
        'status',
        'notes',
    ];

    public function bouquets() {
        return $this->hasMany(BouquetRelation::class, 'resource_id', 'id');
    }

    public function epg() {
        return $this->belongsTo(EpgChannel::class, 'epg_id');
    }

    public function available_bouquets() {
        return $this->bouquets()->where('resource', '=', 'stream');
    }

    public function host() {
        return $this->belongsTo(Host::class);
    }




}
