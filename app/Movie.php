<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

class Movie extends Model implements Auditable
{
    use  \OwenIt\Auditing\Auditable, SoftDeletes;

    use SoftDeletes;

    protected $appends = ['year'];

    protected $fillable = [
        "tmdb_id",
        "title",
        "category_id",
        "movie_link",
        "adult",
        "duration",
        "thumbnail",
        "poster",
        "collection_id",
        "trailer_url",
        "detail",
        "status",
        "home_screen",
        "is_home_screen",
        "rating",
        "released",
        "deleted_by",
        "type",
    ];

    public function collection() {
        return $this->hasOne(Collection::class, 'id', 'collection_id');
    }

    public function genres() {
        return $this->hasMany(GenreMovie::class, 'movie_id', 'id');
    }

    public function getYearAttribute() {
        $explode = explode("-", $this->attributes['released']);
        return  $explode[0] ?? 2019;
    }

    public function bouquets() {
        return $this->hasMany(BouquetRelation::class, 'resource_id', 'id');
    }

    public function available_bouquets() {
        return $this->bouquets()->where('resource', '=', 'movie');
    }

}
