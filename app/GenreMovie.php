<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

class GenreMovie extends Model implements Auditable
{
    use  \OwenIt\Auditing\Auditable, SoftDeletes;

    protected $fillable = [
        "movie_id",
        "genre_id",
        "tmdb_id",
    ];

    public function genre() {
        return $this->hasOne(Genre::class, 'id', 'genre_id');
    }
}
