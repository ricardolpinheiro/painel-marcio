<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;
class TvSerieSeasonEpisode extends Model implements Auditable
{
    use  \OwenIt\Auditing\Auditable, SoftDeletes;

    protected $fillable = [
        "seasons_id",
        "episode_no",
        "title",
        "category_id",
        "episode_link",
        "thumbnail",
        "detail",
    ];

    public function season() {
        return $this->hasOne(TvSerieSeason::class, 'id', 'seasons_id');
    }
}
