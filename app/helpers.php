<?php

use App\Jobs\SyncTMDBEpisode;
use App\Jobs\SyncTMDBSeason;
use App\Jobs\SyncTMDBSerie;
use App\Movie;
use App\TvSerie;
use App\TvSerieSeason;
use App\TvSerieSeasonEpisode;
use Illuminate\Support\Facades\Log;
use Ixudra\Curl\Facades\Curl;

function generateRandomString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

function parseSelectArray($obj, $k, $conc = false, $par = '') {
    $res = ['' => 'Selecione'];
    foreach ($obj as $key => $item) {
        if($conc) {
            $res[$item['id']] = $item[$par] . ' - ' . $item[$k];
        } else {
            $res[$item['id']] = $item[$k];
        }

    }
    return $res;
}

function fetch($url) {
    $response = Curl::to($url)
        ->returnResponseObject()
        ->withTimeout(0)
        ->asJson()
        ->get();

    if($response->status == 200) {
        return $response->content;
    } else {
        Log::info("Status != 200: " . $url);
    }

    return null;
//    try {
//        $search_data = @file_get_contents($url);
//    } catch (\Cassandra\Exception\TimeoutException $e) {
//        Log::info($url);
//    } catch (Exception $e) {
//        Log::info($url);
//    }
//
//
//    if ( $search_data ) {
//        $data = json_decode( $search_data, true );
//        if(isset($data)) {
//            return $data;
//        }
//        return null;
//    }
//    return null;
}

function tmdbGetMovieById($tmdb_id) {
    $url = str_replace(REPLACEMENT_ID, $tmdb_id, TMDB_API_FIND_BY_ID);
    return fetch($url);
}

function tmdbGetMovieByName($title) {
    $url = str_replace(REPLACEMENT_NAME, urlencode($title),TMDB_API_FIND_BY_NAME);
    return fetch($url);
}

function tmdbGetCollectionById($id) {
    $url = str_replace(REPLACEMENT_ID, urlencode($id),TMDB_API_FIND_COLLECTION);
    return fetch($url);
}

function tmdbGetSerieById($tmdb_id) {
    $url = str_replace(REPLACEMENT_ID, $tmdb_id, TMDB_API_FIND_SERIE_BY_ID);
    return fetch($url);
}

function tmdbGetSerieByName($title) {
    $url = str_replace(REPLACEMENT_NAME, urlencode($title), TMDB_API_FIND_SERIE_BY_NAME);
    return fetch($url);
}

function tmdbGetEpisodeBySerie($serie, $season, $episode) {
    $url = str_replace(REPLACEMENT_ID, urlencode($serie), TMDB_API_FIND_EPISODE_BY_SERIE);
    $url = str_replace(REPLACEMENT_SEASON, $season, $url);
    $url = str_replace(REPLACEMENT_EPISODE, $episode, $url);
    return fetch($url);
}


function tmdbGetSeasonBySerie($serie, $season) {
    $url = str_replace(REPLACEMENT_ID, urlencode($serie), TMDB_API_FIND_SEASON_BY_SERIE);
    $url = str_replace(REPLACEMENT_SEASON, $season, $url);
    return fetch($url);
}

//TODO temporaário para importação de dados - Série
function checkExistsSerie($s) {

    if(isset($s['name'])) {
        $data = TvSerie::where('title', '=', trim($s['name']))->orWhere('original_name', trim($s['name']))->get();


        if($data->count() == 0 && !empty($s['name'])) {
            $serie = tmdbGetSerieByName(trim($s['name']));

            if($serie->total_results > 0) {
                $find = TvSerie::where('title', '=', $serie->results[0]->name)->orWhere('original_name', $serie->results[0]->name)->get();

                if($find->count() == 0) {
                    if($serie->total_results > 0) {
                        $create['tmdb_id'] = $serie->results[0]->id;
                        $saved = TvSerie::create($create);
                        dispatch(new SyncTMDBSerie($saved));
                        return $saved;
                    }
                }
            }
        } elseif(!empty($s['name'])) {
            return $data[0];
        } else {
            Log::info("Erro" ,$s);
        }
    } else {
        Log::info("Erro" ,$s);
    }
}
//TODO temporaário para importação de dados - Season
function checkExistsSerieSeason (TvSerie $serie, $s) {
    $season = $serie->seasons()->where('season_no', '=', $s['temporada'])->get();
    if($season->count() == 0) {
        $temp['tv_series_id'] = $serie->id;
        $temp['season_no'] = $s['temporada'];
        $saved = TvSerieSeason::create($temp);
        dispatch(new SyncTMDBSeason($saved));
        return $saved;
    } else {
        return $season[0];
    }
}
//TODO temporaário para importação de dados - Episode
function checkExistsSerieSeasonepisode(TvSerieSeason $season, $serie) {
    $episode = $season->episodes()->where('episode_no', '=', $season['episodio'])->get();

    if($episode->count() == 0) {
        $ep['seasons_id'] = $season->id;
        $ep['episode_no'] = $serie['episodio'];
        $ep['episode_link'] = $serie['link'];
        $saved = TvSerieSeasonEpisode::create($ep);
        dispatch(new SyncTMDBEpisode($season->id, $saved->id));
    }
}

function mergeGenres(Movie $movie) {
    $genres = $movie->genres()->get();
    $arr = [];

    foreach ($genres as $g) {
        if($g->genre()->count() > 0) {
            array_push($arr, $g->genre()->get()[0]->name);
        }
    }

    return implode($arr, ", ");
}
