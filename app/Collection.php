<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

class Collection extends Model implements Auditable
{
    use  \OwenIt\Auditing\Auditable, SoftDeletes;

    protected $fillable = [
        "name",
        "overview",
        "poster_path",
        "backdrop_path",
        "tmdb_id",
    ];

    public function movie() {
        return $this->hasMany(Movie::class, 'collection_id', 'id' );
    }

}
