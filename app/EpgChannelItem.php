<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

class EpgChannelItem extends Model implements Auditable
{
    use  \OwenIt\Auditing\Auditable, SoftDeletes;

    protected $fillable = [
        'id_canal',
        'st_titulo',
        'dh_inicio',
        'dh_fim',
        'id_programa',
        'titulo',
    ];

    protected $dates = ['dh_inicio', 'dh_fim'];
}
