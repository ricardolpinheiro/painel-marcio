<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

class TvSerieSeason extends Model implements Auditable
{
    use  \OwenIt\Auditing\Auditable, SoftDeletes;
    protected $fillable = [
        "tv_series_id",
        "tmdb_id",
        "season_no",
        "poster_path",
        "detail",
        "type",
        "deleted_by",
    ];

    public function episodes() {
        return $this->hasMany(TvSerieSeasonEpisode::class, 'seasons_id', 'id');
    }

    public function serie() {
        return $this->hasOne(TvSerie::class, 'id', 'tv_series_id');
    }
}
