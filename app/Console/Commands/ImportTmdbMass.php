<?php

namespace App\Console\Commands;

use App\Jobs\SyncTMDBMovie;
use App\Movie;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class ImportTmdbMass extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:tmdb';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        ini_set('max_execution_time ',3000);
        $movies = Movie::all();
        $page = 50;
        foreach ($movies as $key => $movie) {
            if(empty($movie->thumbnail) && empty($movie->poster)) {
                Log::info("Importando: " . $movie->id . "-" .$movie->title . PHP_EOL);
                dispatch(new SyncTMDBMovie($movie));
                if($key % 50 == 0) {
                    sleep(2);
                }
            }
        }
        return;
    }
}
