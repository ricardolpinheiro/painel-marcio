<?php

namespace App\Console\Commands;

use App\Jobs\EpgChannel;
use App\Jobs\EpgChannelItem;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class EpgSync extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'epg:sync';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        dispatch(new EpgChannel());
        Log::info('Task EPG');
    }
}
