<?php

namespace App\Console\Commands;

use App\Movie;
use Illuminate\Console\Command;

class GenerateHomeScreen extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:homescreen';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $movies = Movie::where('released', 'like', '%2019%')
            ->where('thumbnail', '!=', null)
            ->where('poster', '!=', null)
            ->where('home_screen', '=', null)
            ->orderBy('id', 'desc')
            ->take(10)
            ->get();

        foreach ($movies as $movie) {
            $html = "<div class=\"movie_card\" id=\"ave\"> <div class=\"blur_back ave_back\"></div><div class=\"info_section\"> <div class=\"movie_header\"> <h1>".$movie->title."</h1> <h4>".$movie->year."</h4> <span class=\"minutes\">".$movie->duration." min</span> <p class=\"type\">".mergeGenres($movie)."</p></div><div class=\"movie_desc\"> <p class=\"text\"> ".$movie->detail." </p></div></div></div>";
            $css = " @import url('https://fonts.googleapis.com/css?family=Montserrat:300,400,700,800'); *{box-sizing: border-box; margin: 0;}html, body{margin: 0; font-family: 'Montserrat', helvetica, arial, sans-serif; font-size: 14px; font-weight: 400;}.movie_card{position: relative; display: block; width: 640px; height: 350px; overflow: hidden; border-radius: 0; -webkit-transition: all 0.4s; transition: all 0.4s;}.movie_card:hover{-webkit-transform: scale(1.02); transform: scale(1.02); -webkit-transition: all 0.4s; transition: all 0.4s;}.movie_card .info_section{position: relative; width: 90%; height: 100%; background-blend-mode: multiply; z-index: 2; border-radius: 0; margin-left: 25%;}.movie_card .info_section .movie_header{position: relative; padding: 25px; height: 40%; margin-left: 10%;}.movie_card .info_section .movie_header h1{color: #fff; font-weight: 400;}.movie_card .info_section .movie_header h4{color: #9ac7fa; font-weight: 400;}.movie_card .info_section .movie_header .minutes{display: inline-block; margin-top: 10px; color: #fff; padding: 5px; border-radius: 0; border: 1px solid rgba(255, 255, 255, 0.13);}.movie_card .info_section .movie_header .type{display: inline-block; color: #cee4fd; margin-left: 10px;}.movie_card .info_section .movie_header .locandina{position: relative; float: left; margin-right: 20px; height: 120px; box-shadow: 0 0 20px -10px rgba(0, 0, 0, 0.5);}.movie_card .info_section .movie_desc{padding: 25px; height: 50%; margin-left: 10%;}.movie_card .info_section .movie_desc .text{color: #cfd6e1;}.movie_card .info_section .movie_social{height: 10%; padding-left: 15px; padding-bottom: 20px;}.movie_card .info_section .movie_social ul{list-style: none; padding: 0;}.movie_card .info_section .movie_social ul li{display: inline-block; color: rgba(255, 255, 255, 0.4); -webkit-transition: color 0.3s; transition: color 0.3s; -webkit-transition-delay: 0.15s; transition-delay: 0.15s; margin: 0 10px;}.movie_card .info_section .movie_social ul li:hover{-webkit-transition: color 0.3s; transition: color 0.3s; color: rgba(255, 255, 255, 0.8);}.movie_card .info_section .movie_social ul li i{font-size: 19px; cursor: pointer;}.movie_card .blur_back{position: absolute; top: 0; z-index: 1; height: 100%; left: 0; background-size: cover; border-radius: 0;}@media screen and (min-width: 768px){.movie_header{width: 70%;}.movie_desc{width: 70%;}.info_section{background: -webkit-gradient(linear, left top, right top, color-stop(50%, #0d0d0c), to(transparent)); background: linear-gradient(to left, #0d0d0c 90%, transparent 100%);}.blur_back{width: 35%; background-position: 100% 10% !important;}}@media screen and (max-width: 768px){.movie_card{width: 95%; margin: 70px auto; min-height: 350px; height: auto;}.blur_back{width: 100%; background-position: 50% 50% !important;}.movie_header{width: 100%; margin-top: 85px;}.movie_desc{width: 100%;}.info_section{background: -webkit-gradient(linear, left bottom, left top, color-stop(50%, #141413), to(transparent)); background: linear-gradient(to top, #141413 50%, transparent 100%); display: inline-grid;}}.ave_back{background: url('".$movie->thumbnail."'); background-size: cover !important; background-position: center center !important;}";
            $google_fonts = "Roboto";

            $data = array('html'=>$html,
                'css'=>$css,
                'google_fonts'=>$google_fonts);

            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL, "https://hcti.io/v1/image");
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));

            curl_setopt($ch, CURLOPT_POST, 1);
// Retrieve your user_id and api_key from https://htmlcsstoimage.com/dashboard
            curl_setopt($ch, CURLOPT_USERPWD, "5cd40988-1214-43f0-be10-0d9b3ae7651e" . ":" . "95677cef-e223-42b3-8151-ecf77a4174bb");

            $headers = array();
            $headers[] = "Content-Type: application/x-www-form-urlencoded";
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

            $result = curl_exec($ch);
            if (curl_errno($ch)) {
                echo 'Error:' . curl_error($ch);
            }
            curl_close ($ch);
            $res = json_decode($result,true);
            $movie->home_screen = $res['url'] . "?height=300";
            $movie->save();
            dd($movie);
        }


    }
}
