<?php

namespace App\Console\Commands;

use App\Movie;
use App\Radio;
use App\Stream;
use App\Util\CsvImporter;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class ImportBaseData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:data';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
            ini_set('auto_detect_line_endings',TRUE);
            ini_set('max_execution_time ',1000000);
            $handle = fopen(public_path('database.csv'),'r');
            $importer = new CsvImporter(public_path('database.csv'),true);

            foreach ($importer->get() as $key => $value) {
                $chave = explode(",", array_key_first($value));
                $data = explode(",", $value[array_key_first($value)]);
                print_r("Importando: " . str_replace("\"", "", $data[1]) . "-" . $key . PHP_EOL );
                if(str_replace("\"", "", $data[1]) === "Live Streams") {

                    $stream=[];
                    $stream['name'] = str_replace("\"", "", $data[2]);
                    $stream['stream_logo'] = str_replace("\"", "", $data[4]);
                    $stream['stream_link'] = str_replace("\"", "", str_replace("\\", "",
                        str_replace('"]"', "", str_replace('"["', "", $data[3]))));

                    Stream::create($stream);
                    unset($stream);
                }

                if (str_replace("\"", "", $data[1]) === "Movies") {
                    $movie=[];

                    if(substr($data[6], 1, 7) === "tmdb_id") {
                        $id = str_replace("\"", "", substr($data[6], 10, strlen($data[6])));
                        if(!empty($id)) {
                            $movie['tmdb_id'] = $id;
                        }
                    }

                    $movie['title'] = str_replace("\"", "", $data[2]);
                    $movie['movie_link'] = "http://51.79.82.9:25461/movie/ricpeludo/288299/"
                        . str_replace("\"", "", $data[0])
                        . substr(substr($data[3], (strlen($data[3])-7), strlen($data[3])), 0, 4);

                    $movie['category_id'] = 1;
                    Movie::create($movie);
                    unset($movie);
                }

                if (str_replace("\"", "", $data[1]) === "Radio") {
                    $radio = [];
                    $radio['name'] = str_replace("\"", "", $data[2]);
                    $radio['stream_logo'] = $data[3];
                    $radio['category_id'] = 1;
                    $radio['stream_link'] = str_replace("\"", "", str_replace("\\", "",
                        str_replace('"]"', "", str_replace('"["', "", $data[2]))));
                    Radio::create($radio);
                    unset($radio);
                }
            }
    }
}
