<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

class Revenda extends Model implements Auditable
{
    use  \OwenIt\Auditing\Auditable, SoftDeletes;

  protected $fillable = [
        'uuid',
		'name',
		'username',
		'app_name',
		'obs',
		'password',
		'has_painel',
		'limit_users',
		'path_logo',
		'path_background',
        'credits',
		'url_painel',
		'own_id',
		'primary_color',
		'secondary_color',
		'status',
		'created_by',
		'expired_at',
		'is_trial',
    ];

    public function setExpiredAtAttribute($value) {
      if(!empty($value)) {
          $date = \Carbon\Carbon::createFromFormat('d/m/Y', $value)->format('Y-m-d');
          $this->attributes['expired_at'] = $date;
      }
    }

    protected $dates = ['expired_at'];

    public function users() {
        return $this->hasMany(UserRevenda::class, 'revenda_id', 'id');
    }

    public function bouquets() {
        return $this->hasMany(BouquetRelation::class, 'resource_id', 'id');
    }

    public function available_bouquets() {
        return $this->bouquets()->where('resource', '=', 'revenda');
    }
}
