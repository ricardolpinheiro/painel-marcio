<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

class Radio extends Model implements Auditable
{
    use  \OwenIt\Auditing\Auditable, SoftDeletes;

    protected $fillable = [
        'name',
        'category_id',
        'epg_id',
        'stream_logo',
        'stream_link',
        'status',
        'notes',
    ];

    public function bouquets() {
        return $this->hasMany(BouquetRelation::class, 'resource_id', 'id');
    }

    public function available_bouquets() {
        return $this->bouquets()->where('resource', '=', 'radio');
    }
}
